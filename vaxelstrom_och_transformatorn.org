:PROPERTIES:
:ID:       ef745f66-a9d6-4f2d-b880-fd8ac2668f9c
:END:
#+title:Växelström och transformatorn
#+LANGUAGE: sv
#+filetags: 
-----
- taggar ::  
-----

* Växelström

Innan man upptäckte induktion använde man batterier för att producera elektricitet. Men tack vare induktion kunde man omvandla rörelseenergi till elektricitet. En konstruktion som utför denna omvandling kallas för en [[id:bb1ee081-c07a-43cb-9603-83da05d074f9][generator]]. En generator har många olika designer, men en av de enklaste består av en slinga som snurrar i ett homogent magnetfält. Denna typ av generator producerar något som vi kallar för växelström. Växelström till skillnad från likström varierar regelbundet med tiden. Till exempel strömmen som vi får från vägguttaget växlar fram och tillbaka (i riktning) 50 ggr per sekund (50 Hz). Vi ska nu se hur en sådan ström kan produceras med hjälp av en enkel modellför en generator.

Om vi placerar en slinga inuti ett homogent magnetfält som i figuren nedan så kan vi kolla på hur det magnetiska flödet varierar då vi snurrar slingan runt en axel.

#+DOWNLOADED: screenshot @ 2021-12-03 15:49:28
[[file:Växelström_och_transformatorn/2021-12-03_15-49-28_screenshot.png]]

Följande simulering från [[https://www.walter-fendt.de/html5/phde/][Walter Fendth]] illustrerar förloppet

#+begin_export html
<style>
    #myFrame { width:100%; height:800px; }
</style>

<iframe id="myFrame" src="https://www.walter-fendt.de/html5/phen/generator_en.htm"  style="border: 2px solid black;"></iframe>

#+end_export


Det magnetiska flödet genom slingan är

$\Phi = BA$

men när slingan roterar kommer bara komposanten av B som är vinkelrät mot slingans plan att bidra till det magnetiska flödet. Vi kan då skriva:

$\Phi = BA\cos{\theta}$

Om vi tänker oss att slingan har vridit sig en vinkel $\theta = \omega t$ på tiden $t$ så får vi

$\Phi = BA\cos{\omega t}$

Den inducerade spänningen är då

#+DOWNLOADED: screenshot @ 2021-12-03 16:03:26
[[file:Växelström_och_transformatorn/2021-12-03_16-03-26_screenshot.png]]

När slingan roterar kommer vi få en inducerad spänning som varierar som en sinusfunktion

#+DOWNLOADED: screenshot @ 2021-12-03 16:10:40
[[file:Växelström_och_transformatorn/2021-12-03_16-10-40_screenshot.png]]

När flödet är som störst så är förändringen som minst. Man brukar skriva amplituden i uttrycket

#+DOWNLOADED: screenshot @ 2021-12-03 16:15:11
[[file:Växelström_och_transformatorn/2021-12-03_16-15-11_screenshot.png]]

$\hat{u}$ uttalas u-topp och kallas för _toppvärdet_.

** Effektivvärde

Att ström och spänning hela tiden varierar i växelström gör det problematiskt att beräkna effekten som utvecklas i en krets. Därför definierar man _effektivvärdet_, vilket motsvarar den likström som behövs för att ge samma effektutveckling i en resistor.

För växelström har vi uttrycket

$i = \hat{i}\sin{\omega t}$

där effektivvärdet är relaterad till toppvärdet som $I = \frac{\hat{u}}{\sqrt{2}}$ 

Motsvarande för växelspänningen har vi

$u = \hat{u}\sin{\omega t}$

där effektivvärdet är relaterat till toppvärdet som $U = \frac{\hat{u}}{\sqrt{2}}$

*** Exempel

I svenska uttag har vi växelspänningen $u = 325\sin{(100\pi t)}$. Hur stor är spänningens frekvens och effektivvärde?

#+DOWNLOADED: screenshot @ 2021-12-03 16:26:57
[[file:Växelström_och_transformatorn/2021-12-03_16-26-57_screenshot.png]]

* Transformatorn

Transformatorn är en apparat som kan förändra förhållandet mellan ström och spänning. Den består av två spolar förbundna med en järnkärna

#+DOWNLOADED: screenshot @ 2021-12-03 16:29:39
[[file:Transformatorn/2021-12-03_16-29-39_screenshot.png]]

Man kan med hjälp av detta välja vilken spänning man vill ha ut. Man kan visa att förhållandet mellan spolarnas lindningsvarv, in och utspäningar, samt strömmar är

#+DOWNLOADED: screenshot @ 2021-12-03 17:28:34
[[file:Transformatorn/2021-12-03_17-28-34_screenshot.png]]

För att se hur, så börjar vi med att kolla på primärspolen som en krets. Om vi antar att resistansen i trådarna är låg så kan vi se primärkopplingen som en spänningskälla i serie med en spole.

#+DOWNLOADED: screenshot @ 2022-12-04 21:17:38
#+ATTR_HTML: :width 850px
 [[file:Transformatorn/2022-12-04_21-17-38_screenshot.png]]

Eftersom vi har en växelspänning som spänningskälla så kommer det induceras en spänning i primärspolen. Denna kommer vara riktad så att det skapas en ström som är motriktad denna. Då kommer spänningen över spolen få motsatt polaritet som spänningskällan.

Om vi gör en potentialvandring runt denna krets så får vi att

$U_1 = e_1$

dvs den inducerade spänningen i primärspolen har samma storlek som spänningskällan.

Strömmen som skapas i _primärspolen_ skapar ett magnetfält som "leds" runt i järnkärnan. På grund av att vi har en växelström varierar magnetfältet och inducerar en ström i sekundärspolen. Denna kommer av samma argument som ovan att vara

$e_2 = U_2$

I de båda spolarna induceras det

#+DOWNLOADED: screenshot @ 2021-12-03 17:22:52
[[file:Transformatorn/2021-12-03_17-22-52_screenshot.png]]

division av dessa ger

#+DOWNLOADED: screenshot @ 2021-12-03 17:23:33
[[file:Transformatorn/2021-12-03_17-23-33_screenshot.png]]

$e_1$ kommer hela tiden att vara $U_1$ och i sekundärspolen får vi att $e_2 = U_2$ vilket ger

#+DOWNLOADED: screenshot @ 2021-12-03 17:25:52
[[file:Transformatorn/2021-12-03_17-25-52_screenshot.png]]

Då har vi första delen av ekvationen ovan. För att se realtionen emd strömmen kan man utnyttja att effekten som man matar in i primärspolen måste vara lika stor som effekten man får ut i sekundärspolen, om vi antar att vi inte har några effektförluster (Effekten kan ju inte heller bli större, för så hade vi ju skapat eneri på något sätt).

Effekten är $P = UI$ vilket innebär att

#+DOWNLOADED: screenshot @ 2021-12-03 17:27:50
[[file:Transformatorn/2021-12-03_17-27-50_screenshot.png]]

och vi har därmed visat att 

#+DOWNLOADED: screenshot @ 2021-12-03 17:28:34
[[file:Transformatorn/2021-12-03_17-28-34_screenshot.png]]

** Exempel

Du vill ladda din telefon som ska anslutas till 5V. Du glömde laddaren hemma, men har tillgång till spolar och järnkärnor. Du tar fram en spole på 500 varv som du kopplar ihop med en järnkärna. Hur många varv ska sekundärspolen ha om du kopplar 230V växelström till spolen för att du ska få ut 5V?

#+DOWNLOADED: screenshot @ 2021-12-03 17:31:46
[[file:Transformatorn/2021-12-03_17-31-46_screenshot.png]]
