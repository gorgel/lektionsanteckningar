:PROPERTIES:
:ID:       a72b053b-fb7a-464c-95f2-99d9869480f1
:END:
#+title: Repetition Ellära del 1 
#+LANGUAGE: sv
#+filetags:  
-----
- taggar ::
-----

* Rep. Ellära del 1 

** Exempel (Elektroskop)

Om vi har en positivt laddad stav och ett neutralt elektroskop

#+DOWNLOADED: screenshot @ 2023-02-08 19:38:26
[[file:Laddning_och_Coulombs_lag/2023-02-08_19-38-26_screenshot.png]]

Om vi sedan för den laddade staven nära plattan kommer elektronerna i elektroskopet att attraheras av staven och röra sig till toppen av elektroskopet.

#+DOWNLOADED: screenshot @ 2023-02-08 19:38:44
[[file:Laddning_och_Coulombs_lag/2023-02-08_19-38-44_screenshot.png]]

När mycket negativ laddning har försvunnit från den nedre delen av elektroskopet så blir de båda delarna positivt laddade och repelerar varndra. 

#+DOWNLOADED: screenshot @ 2023-02-08 19:39:02
[[file:Laddning_och_Coulombs_lag/2023-02-08_19-39-02_screenshot.png]]

** Exempel 


Den övre figuren nedan visar två kulor vardera med laddningen Q och utritade krafter. I den nedre figuren har avståndet förändrats. Rita in krafterna i denna figur. De ska ritas i samma skala som i den övre figuren.

#+DOWNLOADED: screenshot @ 2023-02-21 19:01:16
[[file:Rep._Ellära_del_1/2023-02-21_19-01-16_screenshot.png]]

*** Lösning

#+DOWNLOADED: screenshot @ 2023-02-21 19:06:22
[[file:Rep._Ellära_del_1/2023-02-21_19-06-22_screenshot.png]]

** Exempel

En negativ laddning -q påverkas av två laddningar enligt figuren. Vilken av pilarna i figuren anger riktningen för den resulterande elektriska kraften som verkar på laddning -q? 

#+DOWNLOADED: screenshot @ 2023-02-21 19:08:31
[[file:Rep._Ellära_del_1/2023-02-21_19-08-31_screenshot.png]]

*** Lösning

#+DOWNLOADED: screenshot @ 2023-02-21 19:13:43
[[file:Rep._Ellära_del_1/2023-02-21_19-13-43_screenshot.png]]

Kraften har en riktning som är tangent till flödeslinjen i den punkten. Eftersom laddningen är negativ är riktningen mot fältet och går då till höger.

** Exempel

En elektron rör sig i ett homogent elektriskt fält, parallellt med fältlinjerna enligt figur. Dess elektriska potentiella energi $W_p$ ändras därvid med läget x enligt diagrammet. Beräkna den elektriska fältstyrkan.

#+DOWNLOADED: screenshot @ 2023-02-21 19:16:24
[[file:Rep._Ellära_del_1/2023-02-21_19-16-24_screenshot.png]]

*** Lösning

#+DOWNLOADED: screenshot @ 2023-02-22 08:32:18
[[file:Rep._Ellära_del_1/2023-02-22_08-32-18_screenshot.png]]
