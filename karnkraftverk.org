:PROPERTIES:
:ID:       19962e04-cfcd-443a-9c36-7c60e6208c74
:END:
#+title:Kärnkraftverk
#+LANGUAGE: sv
#+filetags: 
-----
- taggar ::
-----

* Kärnfission

Hur fungerar en kärnreaktor?

Vi vet at vi kan få ut energi om vi klyver tunga kärnor till två mindre. Till exempel så kan man klyva Uran och få ut energi. 

#+DOWNLOADED: screenshot @ 2022-05-05 08:11:35
[[file:Kärnreaktorn/2022-05-05_08-11-35_screenshot.png]]
 
Det finns mer än 300 olika sätt för Uran att klyvas och det kan bli mer än 600 olika dotterkärnor.

Ett exempel på en kärnfission är reaktionen

#+DOWNLOADED: screenshot @ 2022-05-05 08:12:54
[[file:Kärnreaktorn/2022-05-05_08-12-54_screenshot.png]]

Massdefekten och den frigjorda energin för en sådan klyvning är

#+DOWNLOADED: screenshot @ 2022-05-05 08:13:35
[[file:Kärnreaktorn/2022-05-05_08-13-35_screenshot.png]]


1 kg Uran består av ca

#+DOWNLOADED: screenshot @ 2022-05-05 08:15:52
[[file:Kärnreaktorn/2022-05-05_08-15-52_screenshot.png]]
 
Vilket ger en energi på

#+DOWNLOADED: screenshot @ 2022-05-05 08:16:13
[[file:Kärnreaktorn/2022-05-05_08-16-13_screenshot.png]]

Denna mängd energi motsvarar ca den energi man skulle få ut från 1000 ton olja.

* Kärnkraftverk

Ett kärnkraftverk består av 4 viktiga delar. Själva _reaktorn_, en _moderator_, _kontrollstavar_ och _uranbränsle_. Uranbränslet består av stavar av Uran och kallas tillsammans för _härden_.

#+DOWNLOADED: screenshot @ 2022-05-05 08:18:58
[[file:Kärnkraftverk/2022-05-05_08-18-58_screenshot.png]]
 
Vi ska nu titta på dessa komponenters funktioner en i taget.

** Bränslestavarna

Bränslestavarna består till största delen av Uran-238 (ca 97-98%) och en liten den Uran-235. Uran-238 absorberar lätt neutroner med hög energi medans Uran-235 klyvs av "termiska" neutroner, dvs relativt långsamma neutroner. Uranet staplas i stavar med en diameter på ca 1 cm. När Uran-235 klyvs frigörs det energi i forma av rörelseenergi hos fissionsprodukterna. Dessa hinner inte röra sig speciellt långt innan de krockar med en annan partikel, vilket gör att en friktionsvärme uppstår i staven. Staven blir alltså varm och värmer upp det omgivande vattnet.

#+DOWNLOADED: screenshot @ 2022-05-05 08:25:48
[[file:Kärnkraftverk/2022-05-05_08-25-48_screenshot.png]]

För att kedjereaktionen ska ske spontant krävs en viss kritisk massa av uranet. Man vill inte att reaktionen ska ske spontant eftersom det då blir svårt att kontrollera reaktionen. Istället behöver man på konstgjord väg starta upp klyvningsprocessen genom att sätta ner en stav med Californium-252 eller Plutonium-238 så att processen kommer igång utan att den kritiska massan har uppnåtts.

** Moderatorn
Moderatorns uppgift är att bromsa neutronerna så att de blir "termiska". Vatten är en bra sådan moderator, vilket också är praktiskt vid utvinnandet av energin då vattnet värms upp och förångas. Ångan skickas sedan genom en generator och alstrar ström.

Moderatorn består oftast av antingen tungt (D2O) eller lätt vatten (H20).

#+DOWNLOADED: screenshot @ 2022-05-05 08:46:42
[[file:Kärnkraftverk/2022-05-05_08-46-42_screenshot.png]]

När en klyvning sker så skickas en neutron med hög energi ut. Denna neutron kommer att röra sig igenom vattnet och krocka med vattenmolekylerna ett flertal gånger och förlora energi på vägen. När den har bromsats ner till en hastignhet på ca 2200 m/s så är neutronen "termisk" och kan lätt klyva Uran-235. Detta gör att tre nya neutroner skickas ut och vi får en kedjereaktion. Man vill kunna hålla denna process på en sådan nivå att ungefär varje varje ny klyvning klyver en ny Urankärna så att processen håller sig på en stabil nivå. Annars får man en okontollerad kedjereaktion som tillslut gör att bränslet blir så varmt att man får en härdsmälta. För att kontrollera processen använder man sig av styrstavar.

** Styrstavar

Styrstavarna brukar bestå av antingen Bor, Kadmium eller Hafnium och har som uppgift att absorbera neutronerna i reaktorn. Stavarna kan sänkas ned mellan Uranstavarna och absorbera en del av neutronerna i moderatorn. Ju längre ner stavarna sänks desto färre klyvningar kan ske. Så genom att kontrolera hur djupt ner stavarna sänks kan man kontrollera hastigheten på kärnreaktionsprocessen. Har man för många klyvningar så sänker man dem och har man för få så höjer man dem.

#+DOWNLOADED: screenshot @ 2022-05-05 09:54:37
[[file:Kärnkraftverk/2022-05-05_09-54-37_screenshot.png]]

** Generering av elektricitet
Man genererar elektriciteten i ett kärnkraftverk på ett liknande sätt som i andra kraftverk. När Uranbränslet blir varmt så överförs värmen till moderatorn, dvs vattnet. Vattnet når en koktemperatur och förångas. Ångan leds vidare till en turbin som driver en generator. Det är generatorn som sedan omvandlar turbinens rörelseenergi till elektricitet. Ångan som passerar turbinen leds sedan vidare till en kondensor. I kondensorn har man ledningar med kylvatten (oftast taget från havet). Kylvattnet kyler den varma ångan, som då kondenseras och blir vätska igen. Den kalla vätskan leds sedan tillbaka till reaktorn med hjälp av en pump.

#+DOWNLOADED: screenshot @ 2022-05-05 10:00:26
[[file:Kärnkraftverk/2022-05-05_10-00-26_screenshot.png]]

* Produktion av Kärnkraftverk

I diagrammet nedan kan man se en historisk överblick av hur mycket varje reaktor ha rproducerat sen början på 70-talet.

#+DOWNLOADED: screenshot @ 2022-05-05 16:34:05
[[file:Producktion_av_Kärnkraftverk/2022-05-05_16-34-05_screenshot.png]]

De reaktorer som är i drift idag är och produktionsmängden summeras i tabellen nedan.

| Namn       | Reaktor 1 | Reaktor 2 | Reaktor 3 | Reaktor 4 |
|------------+-----------+-----------+-----------+-----------|
| Forsmark   | 990 MW    | 1118 MW   | 1172 MW   | -         |
| Oskarshamn | -         | -         | 1450 MW   | -         |
| Ringhals   | -         | -         | 1062 MW   | 1120 MW   |


I grafen nedan kan vi studera fördelningen av olika kraftkällor i sverige sedan 70-talet.

#+DOWNLOADED: screenshot @ 2022-05-05 16:39:52
[[file:Producktion_av_Kärnkraftverk/2022-05-05_16-39-52_screenshot.png]]
 
* Förvaring

Förvaringen av kärnavfall hanteras i 3 steg.

1. När bränslet är förbrukat så bevaras det först vid kärnkraftverket i basänger under 1 års tid.

2. Sedan transporteras kärnavfallet och mellanlagras i Oskarshamn. Där placeras det i bergrum ca 50 meter under marken. Kärnavfallet har då hög aktivitet och lagras i berggrunden i ca 40 år innan avfallet når en mer hanterbar aktivitetsnivå.

3. När aktiviteten är på hanterbara nivåer så gjuts avfallet in i behållare av plåt och/eller betong tillsammans med cement eller bitumen (ett bindelement i asfalt) för slutförvar i ca 1000-100000 år. Sverige är världsledande i slutförvar av kärnavfall och slutförvaret ska ske i forsmark där det finns 500 tunnlar på 500 meters djup i berget. Där kan 12000 ton kärnbränsle slutförvaras.

Slutförvaret har fått okej av regeringen under 2022, men processen fortsätter med att mark och miljödomstolens ska besluta om tillstånd och villkor.
