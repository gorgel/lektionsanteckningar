
// let elements = document.querySelector(".language-mhtml");
// elements.classList.replace("language-mhtml", "language-html hljs language-xml");

let elements = document.querySelectorAll('.language-mhtml');

// Iterate through each element and update its classes
elements.forEach(element => {
  element.classList.replace('language-mhtml', 'language-html');
  element.classList.add('hljs', 'language-xml');
});

// const element = document.querySelector('.language-mhtml');
// const newClasses = ['language-html', 'hljs', 'language-xml'];

// element.className = ''; // Clear all classes
// element.classList.add(...newClasses); // Add new classes

hljs.initLineNumbersOnLoad();
hljs.highlightAll();
hljs.addPlugin(new CopyButtonPlugin());
