:PROPERTIES:
:ID:       77f2f5de-025e-4d5c-b242-2fa443624d92
:END:
#+title:Momentanhastighet, st- och vt-diagram
#+LANGUAGE: sv
#+filetags:  
-----
- taggar ::
-----

* Repetition medelhastighet

Medelhastigheten i ett intervall får man genom att dividera ändringen i sträcka med ändringen i tid:

$$ v_{med} = \frac{\Delta s}{\Delta t} $$

* Momentanhastighet

Ibland vill man veta hastigheten i ett visst ögonblick. Det kallas _momentanhastighet_ $v_{mom}$.

** Exempel

Usain Bolt springer 100 m på 9,58 s. 

a) Vilken medelhastighet har Usain Bolt i intervallet 2 s <t< 9,58s?

b) Vilken momentanhastighet har Usain Bolt när t = 8 s?

c) När är momentanhastigheten högst: efter 1 s eller efter 8 s?

#+DOWNLOADED: screenshot @ 2022-08-29 20:18:41
[[file:Momentanhastighet/2022-08-29_20-18-41_screenshot.png


*** Lösning

a)

#+DOWNLOADED: screenshot @ 2022-08-29 20:37:03
[[file:Momentanhastighet/2022-08-29_20-37-03_screenshot.png]]


b) Man kan uppskatta momentanhastigheten i en viss punkt genom att rita ut _tangenten_ i punkten man är intresserad av (här s = 8). Den ljusblåa linjen nedan har ritats ut med en linjal (på ett ungefär). *Att beräkna momentanhastigheten är då samma sak som att beräkna lutningen på denna tnagent*.

Detta kan vi göra genom att ta två valfria punkter på kurvan (gärna några som är lätta att läsa av om det går) och beräkna lutningen från dessa punkter.

#+DOWNLOADED: screenshot @ 2022-08-29 20:33:16
[[file:Momentanhastighet/2022-08-29_20-33-16_screenshot.png]]

Här ser vi att momentanhastigheten vid den åttonde sekunden blev ca 22,2 m/s.

c)

Om vi uppskattar momentanhastigheten i punkten t = 1 s får vi


#+DOWNLOADED: screenshot @ 2022-08-29 21:03:47
[[file:Momentanhastighet/2022-08-29_21-03-47_screenshot.png]]

Då ser vi att momentanhastigheten är störst vid t = 8 s och minst vid t = 1 s. Vi ser det också genom att lutningen på tangenten är brantare vid t = 8 s än vid t = 1 s. 

** Övning - Gör uppgift 19 i fysik 1000 (s.6)

#+DOWNLOADED: screenshot @ 2024-08-26 18:27:51
[[file:Momentanhastighet/2024-08-26_18-27-51_screenshot.png]]

*** Lösning

#+DOWNLOADED: screenshot @ 2024-08-26 18:49:38
[[file:Momentanhastighet/2024-08-26_18-49-38_screenshot.png]]

** Övning

Ett föremål rör sig enligt st-diagrammet till nedan:

a ) Vilken momentanhastighet har föremålet till höger när t = 5 s?

b) Vilken momentanhastighet har föremålet till vänster när t = 3 s?

#+DOWNLOADED: screenshot @ 2022-08-29 20:44:17
[[file:Momentanhastighet/2022-08-29_20-44-17_screenshot.png]]

*** Lösning

När föremålet befinner sig på den högra sidan av grafen (område 2, vid t = 5 s) så är momentanhastigheten samma i alla punkter mellan 4 till 12. När föremålet är till vänster (område 1, vid t = 3 s) så är momentanhastigheten en annan men fortfarande samma i alla punkter mellan 0 till 4. Vi kan alltså beräkna lutningarna i område 1 och område 2 för att få motsvarande momentanhastighet. 

#+DOWNLOADED: screenshot @ 2022-08-29 20:49:25
[[file:Momentanhastighet/2022-08-29_20-49-25_screenshot.png]]

* St- och vt-diagram

Lutningen i ett st-diagram motsvarar hastigheten. Vad är motsvarande vt-diagram för st-diagramet?

1.
   
#+DOWNLOADED: screenshot @ 2022-08-29 20:56:29
[[file:St-_och_vt-diagram/2022-08-29_20-56-29_screenshot.png]]

2.

#+DOWNLOADED: screenshot @ 2022-08-29 21:04:28
[[file:St-_och_vt-diagram/2022-08-29_21-04-28_screenshot.png]]

3.

#+DOWNLOADED: screenshot @ 2022-08-29 21:00:47
[[file:St-_och_vt-diagram/2022-08-29_21-00-47_screenshot.png]]

** Övning

Övning: Vilket st-diagram motsvarar vilket vt-diagram? Gör stencil!

*** Lösning

1. A2 B3 C1          
2. A3 B1 C2 
