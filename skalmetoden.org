:PROPERTIES:
:ID:       f29e4571-1e98-4c74-8462-411628c7f217
:END:
#+title:Skalmetoden
#+LANGUAGE: sv
#+filetags: 
-----
- taggar ::
-----

* Skalmetoden

Vissa rotationsvolymer är svåra att beräkna med skivmetoden. Till exempel om vi får en rotationsvolym där vi måste göra "hål" i skivan för att få rätt tvärsnittsarea på volymen, så blir det krångligt. Som ett exempel, betrakta följande kurva

#+DOWNLOADED: screenshot @ 2022-02-05 15:17:55
[[file:Skalmetoden/2022-02-05_15-17-55_screenshot.png]]

Om vi roterar denna kurva runt y-axeln får vi en "donut-liknande" (toroid) volym som är uppritad i videon nedan 

#+begin_export html
<video width="100%" height="500" controls>
  <source src="rotvolym_halv_donut.webm" type="video/webm">
Your browser does not support the video tag.
</video>
#+end_export

Om vi skulle använda skivmetoden så skulle vi behöva skivor med hål i mitten och det är krångligt att använda sådana uttryck och sedan beräkna integralen för att bestämma volymen.

Istället kan vi använda oss av _skalmetoden_ där vi tänker oss att vi istället för skivor använder oss av cylindriska skal som vi sedan summerar ihop för att uppskatta volymen

#+DOWNLOADED: screenshot @ 2022-02-05 15:32:41
[[file:Skalmetoden/2022-02-05_15-32-41_screenshot.png]]

Man kan leka med följande interaktiva program från [[https://math.libretexts.org/][Libretext]] för att få en bild av hur det fungerar

#+begin_export html
<style>
    #myFrame { width:100%; height:600px; }
</style>

<iframe id="myFrame" src="https://c3d.libretexts.org/CalcPlot3D/dynamicFigureWCP/index.html?type=volrev;volrev=x;visible=true;alpha=-1;view=0;top2d=sin(x);bot2d=0;umin=0;umax=pi;axisvar=x;axisval=0;revsliderval=0;grid=15,40;polarform=t;polartop=2;polarbottom=0;polarmin=%CF%80/4;polarmax=3%CF%80/4&type=window;hsrmode=0;nomidpts=true;anaglyph=-1;center=4.6062139622651825,1.8627275745762013,10.455287605644587,1;focus=0,0,0,1;up=-0.12240882009196952,0.9645837843431884,-0.23365402574290284,1;transparent=false;alpha=140;twoviews=false;unlinkviews=false;axisextension=0.7;xaxislabel=x;yaxislabel=y;zaxislabel=z;edgeson=true;faceson=true;showbox=false;showaxes=true;showticks=true;perspective=true;centerxpercent=0.5;centerypercent=0.5;rotationsteps=30;autospin=true;xygrid=false;yzgrid=false;xzgrid=false;gridsonbox=true;gridplanes=false;gridcolor=rgb(128,128,128);xmin=-4;xmax=4;ymin=-2;ymax=2;zmin=-4;zmax=4;xscale=1;yscale=1;zscale=1;zcmin=-4;zcmax=4;zoom=0.6;xscalefactor=1;yscalefactor=1;zscalefactor=1;rotatestart=0;rotatedirectionx=1;rotatedirectiony=0;hidexysliders=true;hidetracevalue=true;hidetracepoint=true;hideaxisofrev=true"  style="border: 2px solid black;"></iframe>

#+end_export

För att hitta den exakta volymen så får vi hitta ett uttryck för cylinderns volym, göra tjockleken på cylinderskalet oändligt tunt och sedan räkna ut integralen. Så hur hittar vi ett uttryck för cylindern som vi sedan kan integrera över?

Vi kan tänka oss att vi klipper isär cylindern och vecklar ut den. Då kommer cylindern att få formen av en rektangel som i figuren nedan

#+DOWNLOADED: screenshot @ 2022-02-05 15:41:44
[[file:Skalmetoden/2022-02-05_15-41-44_screenshot.png]]

där bredden på rektangel blir lika stor som cylinderns omkrets. Höjden blir y-värdet och rektangelns djup motsvaras av $\Delta x$. Volymen på rektangeln blir därför

#+DOWNLOADED: screenshot @ 2022-02-05 15:43:46
[[file:Skalmetoden/2022-02-05_15-43-46_screenshot.png]]

och tänker vi oss nu att vi gör cylinderns tjocklek oändligt liten så kommer $\Delta x \rightarrow dx$ så vi får

#+DOWNLOADED: screenshot @ 2022-02-05 15:44:46
[[file:Skalmetoden/2022-02-05_15-44-46_screenshot.png]]

Den totala volymen får vi sedan genom att summera alla skal, dvs vi räknar ut integralen. Skalmetoden ger oss alltså följande uttryck

#+DOWNLOADED: screenshot @ 2022-02-05 15:45:55
[[file:Skalmetoden/2022-02-05_15-45-55_screenshot.png]]

För att kunna räkna ut integralen måste vi hitta förhållandet mellan x och y.

#+DOWNLOADED: screenshot @ 2022-02-05 15:48:28
[[file:Skalmetoden/2022-02-05_15-48-28_screenshot.png]]

Med vårat exempel ovan där vi hade kurvan $y=f(x) = 4x-x^2$ så ger det förhållandet mellan x och y. Vi måste alltså utföra beräkningen

#+DOWNLOADED: screenshot @ 2022-02-05 15:49:04
[[file:Skalmetoden/2022-02-05_15-49-04_screenshot.png]]

* Övning (3348)

Beräkna volymen av det område som begränsas av kurvan $y = 16 - x^2$, positiva x-axeln och positiva y-axlen och roterar runt y-axlen.

** Lösning

#+DOWNLOADED: screenshot @ 2022-02-05 15:52:56
[[file:Övning_(3348)/2022-02-05_15-52-56_screenshot.png]]

