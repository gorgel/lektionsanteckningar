:PROPERTIES:
:ID:       f1ba77ad-0191-4720-a9e6-2e140839b4e0
:END:
#+title: Aktivitet och halveringstid 
#+LANGUAGE: sv
#+filetags:  
-----
- taggar ::
-----

* Aktivitet och halveringstid

** Sönderfall och halveringstid

Ämnen som sönderfaller spontant (av sig själva) kallas radioaktiva. När de sönderfaller bildas en dotterkärna och strålning utsänds. 

När ämnet sönderfaller minskar antalet kärnor som finns kvar av det ursprungliga ämnet.

#+DOWNLOADED: screenshot @ 2023-04-16 13:41:31
[[file:Aktivitet_och_halveringstid/2023-04-16_13-41-31_screenshot.png]]


Den tid som det tar för hälften av ämnet att sönderfalla kallas en halveringstid.  

** Övning

Halveringstiden för Cs-134 är ungefär 2 år. Anta att du i ett preparat har 10 000 kärnor.

#+DOWNLOADED: screenshot @ 2023-04-16 13:54:34
[[file:Aktivitet_och_halveringstid/2023-04-16_13-54-34_screenshot.png]]

1. Hur många kärnor av Cs-134 finns kvar i ditt preparat efter 4 år?

2. Hur lång tid tar det innan det bara finns kvar 625 kärnor kvar av Cs-134 ?

*** Lösning

#+DOWNLOADED: screenshot @ 2023-04-16 13:54:57
[[file:Aktivitet_och_halveringstid/2023-04-16_13-54-57_screenshot.png]]

#+DOWNLOADED: screenshot @ 2023-04-16 13:55:23
[[file:Aktivitet_och_halveringstid/2023-04-16_13-55-23_screenshot.png]]

** Aktivitet

Aktivitet är ett mått på hur många sönderfall som sker i ett preparat per sekund. Aktiviteten kan beräknas:

$A(t) = \lambda N(t)$

där A är aktiviteten i sönderfall/s

Enheten anges i $[s^{-1}] = [Bq]$ = Becquerel

λ är sönderfallskonstanten som anger sannolikheten att en specifik kärna sönderfaller under nästa sekund

N är antalet kärnor i preparatet

** Övning

I ett preparat finns 100 000 kärnor av isotopen syre-14. Den har sönderfallskonstanten 0,00976/s. Hur stor är aktiviteten i preparatet?

*** Lösning

#+DOWNLOADED: screenshot @ 2023-04-16 14:07:47
[[file:Aktivitet_och_halveringstid/2023-04-16_14-07-47_screenshot.png]]

** Övning

Med sönderfallskonstanten kan man också beräkna halveringstiden:

$T_{1/2} = \frac{ln2}{\lambda}$

Syre-14 hade som sagt sönderfallskonstanten 0,00976/s. Vilken halveringstid har denna isotop?

*** Lösning

#+DOWNLOADED: screenshot @ 2023-04-16 14:11:31
[[file:Aktivitet_och_halveringstid/2023-04-16_14-11-31_screenshot.png]]

** Sönderfallslagen
Antalet kärnor som kvarstår i ett radioaktivt preparat kan beräknas med sönderfallslagen:

$N(t) = N_0 e^{-\lambda t}$ 

där 

$N_0$ är antalet kärnor av ett visst slag från början
λ är sönderfallskonstanten och
N är antalet kvarvarande kärnor efter tiden t

** Exempel
Sr-90 har en halveringstid på 28,79 år. Vid en viss tidpunkt har du ett preparat bestående av 100  μg.

Hur stor massa av Sr-90 återstår efter 40 år?

Hur lång tid tar det tills endast 10 μg Sr-90 återstår i preparatet? 

*** Lösning

#+DOWNLOADED: screenshot @ 2023-04-16 14:39:42
[[file:Aktivitet_och_halveringstid/2023-04-16_14-39-42_screenshot.png]]

#+DOWNLOADED: screenshot @ 2023-04-16 14:40:02
[[file:Aktivitet_och_halveringstid/2023-04-16_14-40-02_screenshot.png]]

#+DOWNLOADED: screenshot @ 2023-04-16 14:40:17
[[file:Aktivitet_och_halveringstid/2023-04-16_14-40-17_screenshot.png]]

#+DOWNLOADED: screenshot @ 2023-04-16 14:40:32
[[file:Aktivitet_och_halveringstid/2023-04-16_14-40-32_screenshot.png]]

** Aktivitetslagen

När antalet kärnor minskar i ett preparat avtar också aktiviteten enligt:

$A(t) = A_0 e^{-\lambda t}$ 

där 

$A_0$ är aktiviteten från början i Bq (sönderfall/sekund)
λ är sönderfallskonstanten och
A är aktiviteten efter tiden t i Bq (sönderfall/sekund)

** Övning

Halveringstiden för en radioaktiv isotop är 55,6 s. Vid en viss tidpunkt uppmättes aktiviteten 30 kBq från ett prov av isotopen. Efter hur lång tid har aktiviteten gått ner till 10 kBq?   

*** Lösning

#+DOWNLOADED: screenshot @ 2023-04-17 18:49:26
[[file:Aktivitet_och_halveringstid/2023-04-17_18-49-26_screenshot.png]]

** Övning

I människokroppen finns den radioaktiva isotopen  1940K. Den totala mängden Kalium hos en person uppskattas till 140 g varav 0,012% utgörs av den radioaktiva isotopen. Beräkna den aktivitet som denna isotop skapar i kroppen. Halveringstiden är 1,3⋅109 år.

*** Lösning

#+DOWNLOADED: screenshot @ 2023-04-17 18:49:44
[[file:Aktivitet_och_halveringstid/2023-04-17_18-49-44_screenshot.png]]
