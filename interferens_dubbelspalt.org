:PROPERTIES:
:ID:       38a5c8e6-3856-4eb4-9b58-fa7fb86d0887
:END:
#+title:Interferens
#+LANGUAGE: sv
#+filetags: 
-----
- taggar ::
-----

* Interferens

Alla typer av vågor har en egenskap som kallas _interferens_. Interferens bygger på principen om _superposition_ och vi har tidigare kollat på fenomenet i avsnittet [[id:c14cd5a1-4e85-46bd-b880-1dc40fbb2076][Vågrörelser]] i form av vågor som möts på strängar.

Antag att vi har en källa som producerar vågor. Det skulle till exempel kunna vara två högtalare som är placerade i varandra (teoretiskt). Om vi skulle sända ut samma ton samtidigt (_koherent_, eller i fas) så skulle vågorna överlappa varandra exakt. I simuleringen nedan har vi ritat upp två teoretiska vågor (sinuskurvor), en orange och en grön. Summan av de två vågorna utgör den blåa vågen. Om vi flyttar slidern för b-värdet så att kurvorna överlappar varandra så får vi den beskrivna situationen ovan. Då ser vi att vågorna _interfererar konstruktivt_ så kallad _konstruktiv interferens_, dvs att den resulterande vågen adderas positivt.

#+begin_export html
<style>
    #geogebra { width:100%; height:600px; }
</style>

<center><iframe id="geogebra" src="https://www.geogebra.org/calculator/nrw6pw93?embed" style="border: 2px solid black;"></iframe></center>
#+end_export

 Om vi sedan flyttar vågen en halv våglängd (som här motsvarar ett värde e$\pi$ på slidern) så ser vi att de båda vågorna kommer adderas destruktivt (adderas negativt) så att vi inte får någon våg alls. Detta kallar vi för _destruktiv interferens_. I våran uppställning så skulle det innebära att om vi skulle flyttat på högtalaren precis en halv våglängd ifrån den andra högtalaren så skulle vi inte höra något ljud alls (Det är via denna princip som brusreducerande hörlurar fungerar). 

Flyttar vi slidern för b-värdet ytterligare med en halv våglängd så är vi tillbaka till konstuktiv interferens. Vi kan summera detta med följande formler

- Konstruktiv interferens då $\Delta s = k\lambda$

- Destruktiv interferens då $\Delta s = \left( k + \frac{1}{2} \right) \lambda$

Där k är ett heltal k = 0, 1, 2, 3, 4..., $\lambda$ är våglängden och $\Delta s$ är _vägskillnaden_ (sträckan den ena vågen är före eller efter den andra). Det vill säga vi får konstruktiv interferens när den ena vågen ligger ett vist antal hela våglängder före eller efter den andra och vi får destruktiv interferens när den ena vågen ligger ett vist antal halva våglängder före eller efter den andra vågen.


** Interferens för två koherenta källor (vattenvågor)

Låt oss säga att vi har en hamn som är skyddad av en mur. I muren finns två öppningar för båtar att ta sig in och ut. Genom dessa öppningar flödar vågor från havet in. När de passerar öppningarna sprider de sig sfäriskt i ett diffraktionsmönster en ligt Hyugens princip ([[id:c2db65ce-2f18-47cf-8ff4-599e5cffe84e][Vattenvågor, brytning och diffraktion]]). Detta gör att vi får två vågfronter som kommer interferera med varandra. På vissa ställen kommer vågen att interferera konstruktivt och på vissa ställen destruktivt. Placerar vi bojar längs en rätvinklig linje på något avstånf från öppningarna kommer bojen som är placerad i centrum mätt från de båda öppningarna att guppa upp och ner mest. Om vi sedan flyttar bojen rätvinkligt ut så kommer det finnas vissa platser som bojen guppar lite och vissa platser där bojen guppar mer. Vi kan se tydligt hur vågorna interfererar med varandra och skapar detta mönster i simuleringen nedan.

#+begin_export html
<style>
    #geogebra { width:100%; height:600px; }
</style>

<center><iframe id="geogebra" src="https://www.geogebra.org/m/XdW2SjQ8" style="border: 2px solid black;"></iframe></center>
#+end_export

Nedan är en skiss på vågfronterna som skapas från källorna (öppningarna i hamnen). Sträckade linjer är vågdalar och fyllda linjer är vågtoppar. Röda linjer är från ena källan och blåa från den andra. Vi ser att vid centrallinjen så bildas ett maximum, dvs här interfererar vågorna så att vi får den största vågen (_centralmax_). Detta på grund av att vågtoppar eller vågdalar överlappar varandra längs denna linje.

Längs den svarta sträckade linjen så kallade nodlinjer kommer en vågtopp och en vågdal att mötas och interferera så att vågen blir platt, dvs vattnet står still (bojen rör sig inte). Detta kalals för det _första minimit_. Fortsätter vi kan vi hitta ytterligare ett maxima som vi kallar _första maxima_. Sen kommer vi till det _andra minimit_ osv. 

#+DOWNLOADED: screenshot @ 2022-03-02 21:30:54
[[file:Interferens/2022-03-02_21-30-54_screenshot.png]]
Detta fungerar på precis samma sätt för ljus om för vattenvågor eller vilka andra typer av vågor som helst. Vi får dessa min och max pga att vågorna färdas olika långt till bojarna. Vi ska kolla närmare på detta i nästa avsnitt, men då ska vi kolla på ljus istället.

** Exempel

En hamn har två hål i piren som skyddar hamnen. Avståndet mellan de två hålen är 4m.  Vågor som rör sig med en hastighet av 2 m/s kommer i från havet och diffrakterar i de två hålen. 12 m från hålen ligger en boj och guppar i centralmaximat (se figur). Du tar bojen och flyttar den i sidled och märkar att guppningen först avtar men sedan ökar igen och når ett maximum då du har simmat 3.1 meter vinkelrät ut från ursprungspunkten. Mha denna information, bestäm vågornas frekvens.

#+DOWNLOADED: screenshot @ 2022-03-03 18:05:35
[[file:Interferens/2022-03-03_18-05-35_screenshot.png]]

*** Lösning

#+DOWNLOADED: screenshot @ 2022-03-03 18:11:01
[[file:Interferens/2022-03-03_18-11-01_screenshot.png]]



