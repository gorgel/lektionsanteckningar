:PROPERTIES:
:ID:       5054df42-6e65-4e39-a86c-254b694505e2
:END:
#+title: Newtons gravitationslag 
#+LANGUAGE: sv
#+filetags:  
-----
- taggar ::
-----

* Allmänna gravitationslagen

Allmänna gravitationslagen eller _Newtons gravitationslag_ är: 

$F = G\frac{m_1 m_2}{r^2}$

#+DOWNLOADED: screenshot @ 2022-09-26 12:48:01
[[file:2022-09-26_12-48-01_screenshot.png]]

där G är gravitationskonstanten, $G = 6,674 \cdot 10^{-11}$ $Nm^2/kg^2$ 

$m_1$ är massan för ena kroppen

$m_2$ är massan för den andra kroppen

$r$ är avståndet från masscentrum av den ena kroppen till masscentrum av den andra kroppen.

** Hitta i formelsamlingen

Allmänna gravitationslagen s.33

Gravitationskonstanten bland naturkonstanter s.3 eller 33

Astronomiska data s.31

** Exempel

a) Hur stor är gravitationskraften som jorden utövar på månen?


b) Hur stor är gravitationskraften som månen utövar på jorden?


c) Kommentar?

#+DOWNLOADED: screenshot @ 2022-09-26 12:52:50
[[file:Allmänna_gravitationslagen/2022-09-26_12-52-50_screenshot.png]]

*** Lösning


#+DOWNLOADED: screenshot @ 2022-09-27 08:53:54
[[file:Allmänna_gravitationslagen/2022-09-27_08-53-54_screenshot.png]]


** Exmepel

Uppskatta hur stor gravitationskraften är mellan 

a) dig och en kompis som sitter bredvid

b) mellan två elektroner på avståndet. 

*** Lösning

#+DOWNLOADED: screenshot @ 2022-09-27 08:54:39
[[file:Allmänna_gravitationslagen/2022-09-27_08-54-39_screenshot.png]]


** Avståndsverkan

I exempel 1 är kraften F mellan vikterna. Om kraften är F i första exemplet, vad blir kraften i de övriga exemplen nedan?

#+DOWNLOADED: screenshot @ 2022-09-26 12:54:19
[[file:Allmänna_gravitationslagen/2022-09-26_12-54-19_screenshot.png]]

*** Lösning

#+DOWNLOADED: screenshot @ 2022-09-27 09:03:02
[[file:Allmänna_gravitationslagen/2022-09-27_09-03-02_screenshot.png]]


* Elefanten och råttan faller lika fort

Härledning av g:

#+DOWNLOADED: screenshot @ 2022-09-26 12:55:44
[[file:Elefanten_och_råttan_faller_lika_fort/2022-09-26_12-55-44_screenshot.png]]
