:PROPERTIES:
:ID:       deft09764f-fb2a-4f42-a9bd-7d909912e0cc
:END:
#+title: programmering 1 - dictionaries 
#+LANGUAGE: sv
#+filetags:  
-----
- taggar ::
-----

* Python dictionaries

** Nyckel-värdepar

I programmering behöver man ofta använda olika datastrukturer för att spara och modifiera information på olika sätt. Vi har tidigare kollat på listor som är en användbar datastruktur, speciellt om man behandlar *ordnad* information. För andra typer av problem kan det vara bättre att använda en datastruktur som *relaterar* olika data med varandra. I sådana fall så använder vi Pythons *dictionaries.*

Ett sådant exempel är om vi till exempel vill spara information om en användare. 

#+begin_src python :exports both :results output
personer = {"Simon": [38, "Lärare", "Fysik"], "Leif": [55, "Chaufför", "Buss"]}
#+end_src

I en dictionary är informationen sparad i nyckel- och värdepar. Här är nycklarna *"Simon"* och *"Leif"*
med motsvarande värden *[36, "Lärare", "Fysik"]*, *[55, "Chaufför", "Buss"]*. Nyckeln "Simon" har alltså ett värde associerat med sig som är en lista med ytterligare information om personen, där index 0 i listan är åldern, index 1 är yrket och index 2 är ämnet.

För att komma åt värde till en nyckel skriver vi *variabelnam[Nyckeln]*, dvs variabeln som innehåller en dictionary följt av hakparenteser med nyckeln inuti

#+begin_src python :exports both :results output
personer = {"Simon": [38, "Lärare", "Fysik"], "Leif": [55, "Chaufför", "Buss"]}

print(personer["Simon"])
#+end_src

#+RESULTS:
: [36, 'Lärare', 'Fysik']

nyckeln kan vara antingen ett heltal, float, sträng, eller boolsk operator (True eller False), men vanligast är att man använder en sträng. Värdet kan däremot vara vilket objekt som helst i python, en sträng, ett tal, lista, dictionary, funktion, etc.

#+begin_src python :exports both :results output
personer = {"Simon": [38, "Lärare", "Fysik"], 2 : print }

print(personer[2])
#+end_src

#+RESULTS:
: <built-in function print>

** Lägga till ett nyckel-värdepar

Man kan lägga till ett nyckel-värdepar genom *variabelnamn[nyckel] = värde* 

#+begin_src python :exports both :results output
personer = {"Simon": [38, "Lärare", "Fysik"], "Leif": [55, "Chaufför", "Buss"]}

personer["Josefin"] = [38, "Arkitekt", "Skrapor"]
print(personer)
#+end_src

#+RESULTS:
: {'Simon': [38, 'Lärare', 'Fysik'], 'Leif': [55, 'Chaufför', 'Buss'], 'Josefin': [38, 'Arkitekt', 'Skrapor']}

** Ändra ett värde

På samma sätt kan man ändra ett värde som redan är kopplat med en nyckel.

#+begin_src python :exports both :results output
personer = {"Simon": [38, "Lärare", "Fysik"], "Leif": [55, "Chaufför", "Buss"]}

personer["Simon"] = {"färg": "gul"} 
print(personer)
#+end_src

#+RESULTS:
: {'Simon': {'färg': 'gul'}, 'Leif': [55, 'Chaufför', 'Buss']}

** Ta bort ett nyckel-värdepar

För att ta bort ett objekt ur en dictionary så kan vi använda pop-funktionen som tar bort värdeparet men samtidigt returnerar värdet.

#+begin_src python :exports both :results output
personer = {"Simon": [38, "Lärare", "Fysik"], "Leif": [55, "Chaufför", "Buss"]}

personer.pop("Simon")
print(personer)
#+end_src

#+RESULTS:
: {'Leif': [55, 'Chaufför', 'Buss']}

** Övning

Fråga fem kamrater om deras favoritband och lagra perosnernas namn som nyckel och värdet som bandnamnet.

a) Printa sedan alla personers favoritband.

b) Ändra en person favoritband genom att modifiera dictionaryn och printa sedan på nytt.

c) Ta bort en person från dictionaryn och printa informationen igen

** Loopa genom dictionaries

Att loopa igenom en dictionaries nycklar är standardsätttet. 

#+begin_src python :exports both :results output
favoritnummer = {"Person 1": 7, "Person 2": 16, "Person 3": 117}

for key in favoritnummer:
    print(key)
#+end_src

#+RESULTS:
: Person 1
: Person 2
: Person 3

Det går att loopa genom alla nyckel och värdepar samtidigt

#+begin_src python :exports both :results output
favoritnummer = {"Person 1": 7, "Person 2": 16, "Person 3": 117}

for key, value in favoritnummer.items():
    print(key, value)
#+end_src

#+RESULTS:
: Person 1 7
: Person 2 16
: Person 3 117

Det går också att loopa igenom bara nycklarna

#+begin_src python :exports both :results output
favoritnummer = {"Person 1": 7, "Person 2": 16, "Person 3": 117}

for key in favoritnummer.keys():
    print(key)
#+end_src

#+RESULTS:
: Person 1
: Person 2
: Person 3

Eller bara värdena

#+begin_src python :exports both :results output
favoritnummer = {"Person 1": 7, "Person 2": 16, "Person 3": 117}

for value in favoritnummer.values():
    print(value)
#+end_src

#+RESULTS:
: 7
: 16
: 117


** Övning

Nedan finns en lista med frukter där fruktens namn är nyckeln i en dictionary och värdet är en lista med två element. Det första elementet anger när frukten är i säsong och det andra elementet är antalet frukter. 

#+begin_src python :exports both :results output

frukt_data = {
    "äpple": ["höst", 5],
    "banan": ["vår", 1],
    "körsbär": ["sommar", 3],
    "dadlar": ["höst", 8],
    "fikon": ["höst", 4],
    "druva": ["höst", 6],
    "kiwi": ["vinter", 5],
    "citron": ["vår", 7],
    "lime": ["vår", 8],
    "mango": ["sommar", 5],
    "apelsin": ["vinter", 2],
    "papaya": ["sommar", 3],
    "persika": ["sommar", 8],
    "päron": ["höst", 3],
    "ananas": ["vår", 5],
    "plommon": ["sommar", 7],
    "granatäpple": ["höst", 3],
    "hallon": ["sommar", 4],
    "jordgubbe": ["vår", 3],
    "vattenmelon": ["sommar", 5]
}

#+end_src


Skapa en funktion som tar en sådan dictionary tillsammans med en årstid och returnerar en dictonary med formatet

#+begin_src python :exports both :results output

{
    "frukter i säsong" : ["frukt1", "frukt2", "..."],
    "totalt antal frukter" : heltal
}

#+end_src

*** Lösning

#+begin_src python

frukt_data = {
    "äpple": ["höst", 5],
    "banan": ["vår", 1],
    "körsbär": ["sommar", 3],
    "dadlar": ["höst", 8],
    "fikon": ["höst", 4],
    "druva": ["höst", 6],
    "kiwi": ["vinter", 5],
    "citron": ["vår", 7],
    "lime": ["vår", 8],
    "mango": ["sommar", 5],
    "apelsin": ["vinter", 2],
    "papaya": ["sommar", 3],
    "persika": ["sommar", 8],
    "päron": ["höst", 3],
    "ananas": ["vår", 5],
    "plommon": ["sommar", 7],
    "granatäpple": ["höst", 3],
    "hallon": ["sommar", 4],
    "jordgubbe": ["vår", 3],
    "vattenmelon": ["sommar", 5]
}

def filtrera_frukter(data, säsong):
    frukter = []
    antal = 0
    for frukt, data in frukt_data.items():
        frukt_säsong = data[0]
        antal_frukter = data[1]
        if frukt_säsong == säsong:
            frukter.append(frukt)
            antal = antal + antal_frukter
    
    ny_frukt_data = {
        "frukter i säsong" : frukter,
        "totalt antal frukter" : antal
        }
        
    return ny_frukt_data

#+end_src
** Nästla dictionaries

Låt oss säga att vi har sparat information för olika användare på en hemsida. Vi kan nästla informationen genom att ha dictionaries i dictionaries. Vi sparar informationen av användarna, dvs förnamn, efternamn och email som en dictionary. Denna dictionary kan vi komma åt med användarnamnet som nyckel. För att printa alla information kan vi loopa över dictionaryn. 

#+begin_src python :exports both :results output
fysiker = {
    "einstein": {
        "förnamn": "Albert",
        "efternamn": "Einstein",
        "email": "albert@tg.se",
    },
    "curie": {
        "förnamn": "marie",
        "efternamn": "curie",
        "email": "curie@tg.se",
    },
    "newton" : {
        "förnamn": "Isac",
        "efternamn": "Newton",
        "email": "f=ma@tg.se",
   } 
}

for username, info in fysiker.items():
    print(username, ":")
    print(info["förnamn"], info["efternamn"], info["email"])
#+end_src

#+RESULTS:
: einstein :
: Albert Einstein albert@tg.se
: curie :
: marie curie curie@tg.se
: newton :
: Isac Newton f=ma@tg.se

** Övning

Skapa ett program där man matar in epostadresser i ett register (de lagras i en dictionary). Nyckeln ska vara ett namn och värdet ska vara en epostadress. I programmet ska man kunna göra följande (Användaren ska få upp en meny med dessa val):

1. Lägg till en address
2. Ändra en adress
3. Ta fram en adress från ett namn
4. Ta bort en adress ur listan
5. visa hela registret
6. Avsluta programmet
