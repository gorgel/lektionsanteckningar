:PROPERTIES:
:ID:       4503e38b-1161-4440-ac1e-227352229b44
:END:
#+title: Prog1 Nummer 
#+LANGUAGE: sv
#+filetags:  
-----
- taggar ::
-----


* Nummer

I denna lektion och några kommande ska vi gå igenom Pythons grundläggande datastrukturer och några tillhörande operationer som man kan utföra på dessa. Vi börjar med att kolla på aritmetiken.

** Heltal och flyttal
:PROPERTIES:
:ID:       0fb5bc22-20fc-4ad5-a6b3-bcc9a07fccef
:END:


Om vi öppnar en REPL och skriver en etta i den 

#+begin_src python :exports both :results output
>>> 1 + 2
#+end_src

#+begin_src python :exports results :results output
print(1)
#+end_src

#+RESULTS:
: 1

Så ser vi att vi får tillbaka siffran 1. Att skriva något i REPL:n och trycka enter innebär att vi *evaluerar* ett uttryck i Python. När vi evaluerar ett uttryck så utför Python någon form av beräkning som beror på uttrycket vi matar in.

I detta fall så ser vi att heltal i Python evalueras till sig själv.

Om vi provar att skriva in ett decimaltal?

#+begin_src python :exports both :results output
>>> 1.234
#+end_src

#+begin_src python :exports results :results output
print(1.234)
#+end_src

#+RESULTS:
: 1.234

Vi ser vi att detta också evaluerar till sig själv.

** Aritmetiska operationer

Addition utför vi med +

#+begin_src python :exports both :results output
>>> 2 + 5
#+end_src

#+begin_src python :exports both :results output
print(2+5)
#+end_src

#+RESULTS:
: 7

Subtraktion utför vi med -

#+begin_src python :exports both :results output
>>> 5 - 2
#+end_src

#+begin_src python :exports both :results output
print(5-2)
#+end_src

#+RESULTS:
: 3

Division utför vi med /

#+begin_src python :exports both :results output
10 / 2
#+end_src

#+begin_src python :exports both :results output
print(10/2)
#+end_src

#+RESULTS:
: 5.0

Observera att vi fick ett flyttal fast vi dividerade två heltal. 

Om vi vill dela två heltal och få ett heltal tillbaka kan vi använda heltalsdivision //

#+begin_src python :exports both :results output
>>> 10 // 2
#+end_src

#+begin_src python :exports both :results output
print(10//2)
#+end_src

#+RESULTS:
: 5

Om heltalsdivisionen inte går jämt upp så ignoreras resten

#+begin_src python :exports both :results output
>>> 10 // 3
#+end_src

#+begin_src python :exports both :results output
print(10//3)
#+end_src

#+RESULTS:
: 3

Multiplikation utför vi med *

#+begin_src python :exports both :results output
>>> 10*2
#+end_src

#+begin_src python :exports both :results output
print(10*2)
#+end_src

#+RESULTS:
: 20

** Expressions (uttryck)

Vi kan också kombinera uträkningar.

#+begin_src python :exports both :results output
>>> (3 + 4) * (5 + 1)
#+end_src

#+begin_src python :exports both :results output
print((3 + 4) * (5 + 1))
#+end_src

#+RESULTS:
: 42

I denna utyräkning har vi flera olika delar som har olika namn:

*Expression*: en uträkning skriven i programmeringsspråkets notation. I uttrycket ovan är 4, 5 + 1, och (3 + 4) * (5 + 1) alla expressions.

*Value*: ett expression 

    Value: an expression that can’t be computed further (it is its own result)

    So far, the only values we’ve seen are numbers.

    Program: a sequence of expressions that you want to run

#+begin_src python :exports both :results output
print(10 + 6/3 -2*2)
#+end_src

#+RESULTS:
: 8.0

Andra aritmetiska operatorer är modulo % (ger resten vid division) och heltalsdivision // (antal gånger som heltalsdivisionen går)

#+begin_src python :exports both :results output
print(21%5)
print(21//5)
#+end_src

#+RESULTS:
: 1
: 4

Upphöjt med kan vi skriva som ** (dubbelstjärna)

#+begin_src python :exports both :results output
print(10**5)
#+end_src

#+RESULTS:
: 100000

Prioriteten för de olika operationerna är

1. Exponentieringar
2. multiplikationer
3. divisioner
4. modulooperationer
5. addition och subtraktion

När vi har operationer med samma prioritet så följer de varandra från vänster till höger.

Om vi vill dela 10 med 2 multiplicerat med 5 så kan vi alltså inte göra såhär

#+begin_src python :exports both :results output
print(10/2*5)
#+end_src

Utan då måste vi använda oss av parenteser

#+RESULTS:
: 25.0

#+begin_src python :exports both :results output
print(10/(2*5))
#+end_src

#+RESULTS:
: 1.0





Vi provar några andra varianter

#+begin_src python :exports both :results output
7/3
2**-4
109201494739427.4248374289738927342
2**-600
3847289478278472384720837428973434253.234828942734897294
#+end_src

#+begin_src python :exports both :results output
print(7/3)
print(2**-4)
print(109201494739427.4248374289738927342)
print(2**-600)
print(3847289478278472384720837428973434253.234828942734897294)
#+end_src

#+RESULTS:
: 2.3333333333333335
: 0.0625
: 109201494739427.42
: 2.409919865102884e-181
: 3.8472894782784724e+36

Python skriver väldigt stora och väldigt små tal på exponentform.

Flyttal som råkar bli ett heltal skrivs ut med en nolla.

#+begin_src python :exports both :results output
print(10.0)
print(20/2)
#+end_src

#+RESULTS:
: 10.0
: 10.0

Det går bra att blanda heltal och flyttal men resultatet kommer alltid att bli ett flyttal.

Decimaltal kallar man i Python och många andra språk för flyttal (floats).

I många andra språk behöver man tänka på om tal är representerade i minnet med ett visst antal bytes och då finns ofta en övre gräns på 8 byte vilket man då kan representera tal upp till $2^{64}-1$

#+begin_src python :exports both :results output
print(2**64-1)
#+end_src

#+RESULTS:
: 18446744073709551615

Men i python behöver vi inte tänka på detta då python sköter det automatiskt åt oss. Det finns inte heller någon övre gräns på hur stora talen kan vara, förutom tills minnet tar slut.

#+begin_src python :exports both :results output
print(2**600)
#+end_src

#+RESULTS:
: 4149515568880992958512407863691161151012446232242436899995657329690652811412908146399707048947103794288197886611300789182395151075411775307886874834113963687061181803401509523685376



Nu ska vi börja göra våra första program, där vi kommer att använda några av de enklaste inbyggda datatyperna.

Addition utför vi med + och subtraktion utför vi med -

#+begin_src python :exports both :results output
print(2+5)
#+end_src

#+RESULTS:
: 7

Division utför vi med / och multiplikation med *

#+begin_src python :exports both :results output
print(10/2)
print(10*2)
#+end_src

#+RESULTS:
: 5.0
: 20

Vi kan också kombinera dessa

#+begin_src python :exports both :results output
print(10 + 6/3 -2*2)
#+end_src

#+RESULTS:
: 8.0

Andra aritmetiska operatorer är modulo % (ger resten vid division) och heltalsdivision // (antal gånger som heltalsdivisionen går)

#+begin_src python :exports both :results output
print(21%5)
print(21//5)
#+end_src

#+RESULTS:
: 1
: 4

Upphöjt med kan vi skriva som ** (dubbelstjärna)

#+begin_src python :exports both :results output
print(10**5)
#+end_src

#+RESULTS:
: 100000

Prioriteten för de olika operationerna är

1. Exponentieringar
2. multiplikationer
3. divisioner
4. modulooperationer
5. addition och subtraktion

När vi har operationer med samma prioritet så följer de varandra från vänster till höger.

Om vi vill dela 10 med 2 multiplicerat med 5 så kan vi alltså inte göra såhär

#+begin_src python :exports both :results output
print(10/2*5)
#+end_src

Utan då måste vi använda oss av parenteser

#+RESULTS:
: 25.0

#+begin_src python :exports both :results output
print(10/(2*5))
#+end_src

#+RESULTS:
: 1.0

