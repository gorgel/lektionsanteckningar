:PROPERTIES:
:ID:       c2db65ce-2f18-47cf-8ff4-599e5cffe84e
:END:
#+title:Vattenvågor, brytning och diffraktion
#+LANGUAGE: sv
#+filetags: 
-----
- taggar ::
-----

* Vattenvågor

Varför bildas vågor på vattnet och hur bildas dem?

Vattenvågor bildas av vinden. När vinden kommer i kontakt med vattnet överför vinden energi till ytan av vattnet. Så länge vinden blåser på vattnet kommer energi överföras. Eftersom det i stort sätt inte finns något som bromsar vågornas rörelse så kommer vågorna sakta att växa i storlek sålänge vinden blåser.

Det finns två saker som avgör storleken på vågorna. Det första är hastigheten på vinden. Ju större hastighet desto mer energi kan överföras. Det andra är under hur lång sträcka som vinden blåser på vattnet. Ju längre sträckan är desto längre tid är vinden i kontakt med vattnet och överför sin energi. Energin som överförs får ytvattnet att börja röra på sig. Men partiklarna i vattnet kommer inte röra sig framåt som man kanske skulle kunna tro. Varje partikel i vattnet kommer att röra sig en cirkelbana. Vi kan se detta tydligt i följande simulering från [[https://ophysics.com/index.html][Ophysics]].

#+begin_export html
<style>
    #ophysics { width:100%; height:600px; }
</style>

<center><iframe id="ophysics" src="https://www.geogebra.org/material/iframe/id/csgxe7t8/width/732/height/700/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false" style="border: 2px solid black;"></iframe></center>
#+end_export

Partiklarna vid ytan rör sig i störst cirkelbana och sen så minskar cirkelbanan med djupen. Vid cirka en halv våglängd ned slutar rörelsen. Vattenvågor kan man se som att de är _båda transversella och longitudinella_ på en och samma gång. vattnet rör sig inte frammåt, utan bara runt, men energin är det som fortplantas.

#+begin_export html

<iframe src="https://drive.google.com/file/d/1aHfJAiecmC_uB9RI1_z8etptUfPPJyPJ/preview" width="640" height="480" allow="autoplay"></iframe>

#+end_export

Att energin inte når längre ner än halva våglängden av vågorna gör att vi får spännande fenomen när vågorna närmar sig kusten då avståndet ned till botten blir mindre än en halv våglängd. Vi kan dela upp vattenvågornas beteende i tre zoner enligt figuren nedan.

I zon 1, ute på det öppna havet, beror vågornas hastighet på våglängden enligt

$$ v = \sqrt{\frac{g\lambda}{2\pi}} $$

#+DOWNLOADED: screenshot @ 2022-02-22 21:53:44
[[file:Vattenvågor/2022-02-22_21-53-44_screenshot.png]]

När vågorna når zon 2, där avståndet ner till botten är mindre än en halv våglängd så kommer partiklarna i vattnet närmast botten inte längre kunna röra sig i en cirkel och kommer skrapa mot botten fram och tillbaka. Detta gör att vågen bromsas in, dvs hastigheten minskar. Vi kom fram till i avsnitten [[id:81e72887-ad59-4c1f-be3f-2cc7d814210e][Ljud och hörsel]] att energin i en transversell våg gavs av

#+DOWNLOADED: screenshot @ 2022-01-26 21:43:53
[[file:Ljudstyrka/2022-01-26_21-43-53_screenshot.png]]

dvs bla av amplituden och våglängden. Det betyder att när hastigheten på vågen minskar så kommer amplituden på vågen att öka. Det innebär att i zon 2 så kommer _hastigheten på vågorna minska, våglängden kommer bli mindre och vågorna högre_.

I zon 3, när avståndet till botten är mindre än ungefär en tjugondels våglängd kommer hastigheten  på vågorna att ges av ekvationen

$$ v = \sqrt{gh} $$

Detta kommer innebära att ytvattnet kommer att röra sig snabbare än vattnet nära botten, vilket göra att _vågen kommer brytas_. Den övre delen kommer åka ifrån den undre.

Nedanstående video ger en väldigt bra genomgång på hur vattenvågor fungerar.


* Gemensamma egenskaper för all vågrörelse

Alla typer av vågfenomen har tre gemensamma egenskaper:

- brytning
- diffraktion
- interferens

Vi ska här kolla närmare på två utav dem, brytning och diffraktion

** Brytning

Ett vågfenomen som man kan upptäcka om man är observant, är att vågorna som kommer in vid en strand oftast kommer in helt parallellt med stranden, även fast vågorna från början kommer in mot strand med en viss vinkel.

I bilden nedan kan vi se ett tydligt exempel på detta.

#+DOWNLOADED: screenshot @ 2022-02-23 09:05:57
[[file:Gemensamma_egenskaper_för_all_vågrörelse/2022-02-23_09-05-57_screenshot.png]]

Hur förklarar vi detta?

Vi kan förklara detta genom att vi vet att vågorna bromsas upp när de närmar sig land. När den delen av vågen som är närmast land kommer in på grundare vatten saktas den delen ned, medans övriga delen av vågen fortsätter med högre hastighet. På så sätt hinner den yttre delen för det mesta ikapp den delen av vågen som börjar närmare stranden. Resultatet blir därför att vågorna i stort sätt kommer in parallellt mot stranden. Detta fenomen kallas att vågorna _bryts_ och är något som påverkar alla vågor som passerar mellan två medium.

I figuren nedan så har vi en inkommande våg i medium 1 som färdas in i ett medium två med en vinkel $i$ ( _infallsvinkeln_ ) mot skiljelinjen mellan medierna. I medium 1 färdas vågen med en hastighet $v_1$ och i medium 2 med hastighet $v_2$. När vågen når skiljelinjen mellan medierna så kommer den ena delen att bromsas in eftersom hastigheten är mindre i medium 2. Detta gör att vågen bryts och får en annan vinkel $b$ ( _brytningsvinkel_ ). Frekvensen på vågen kommer inte ändras men eftersom hastigheten på vågen ändras måste även våglängden ändras.

Om man utgår från trianglarna i figuren så kan man ställa upp ett samband mellan vinklarna och hastigheterna. Under en tid t då en del av vågen passerar mediet (röd triangel) så kommer vågen att ha färdats sträckan $v_2 t$ (lila triangel). På samma tid har delen av vågen som inte passerat mediet färdats sträckan $v_1 t$. 

#+DOWNLOADED: screenshot @ 2022-02-23 16:10:44
[[file:Gemensamma_egenskaper_för_all_vågrörelse/2022-02-23_16-10-44_screenshot.png]]

Vi får de två sambanden

#+DOWNLOADED: screenshot @ 2022-02-23 16:26:53
[[file:Gemensamma_egenskaper_för_all_vågrörelse/2022-02-23_16-26-53_screenshot.png]]

Sätter vi uttrycken lika med varandra får vi

#+DOWNLOADED: screenshot @ 2022-02-23 16:30:01
[[file:Gemensamma_egenskaper_för_all_vågrörelse/2022-02-23_16-30-01_screenshot.png]]

*** Exempel
En våg rör sig genom ett medium där hastigheten på vågen är 3 m/s och bryts in i ett medium 2 med hastigheten 1,3 m/s. Infallsvinkeln är 38 grader. Hur stor är brytningsvinkeln?

#+DOWNLOADED: screenshot @ 2022-02-24 09:35:27
[[file:Gemensamma_egenskaper_för_all_vågrörelse/2022-02-24_09-35-27_screenshot.png]]


** Diffraktion

När vågor passerar genom en öppning kommer vågorna att breda ut sig sfäriskt som i bilden nedan.

#+DOWNLOADED: screenshot @ 2022-02-23 16:34:22
[[file:Gemensamma_egenskaper_för_all_vågrörelse/2022-02-23_16-34-22_screenshot.png]]

Följande videoklipp demonstrarar effekten bra

#+begin_export html
<iframe width="560" height="315" src="https://www.youtube.com/embed/2TMR-EyF_ds" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
#+end_export


Detta fenomen kallas diffraktion och är den andra egenskapen som alla vågor har. Man kan förklara den genom en princip som kallas _Huygens princip_.

Huygens princip går ut på att vågfronten består av många punktkällor som hela tiden skickar ut nya sfäriska vågor som sprider sig i alla riktningar. När vågorna är plana (som innan öppningen i figuren nedan) bildar alla nya små vågfronter en ny stor som då är parallell med den tidigare vågfronten. När vågfronten passerar öppningen så kommer vågen att kapas av. Punktkällorna precis vid öppningens kanter kommer då att skapa en vågfront som inte längre är plana utan mera rundade. Denna rundade vågfront kommer sedan att skicka ut nya vågfronter från sina punktkällor osv, vilket gör att vi får en vågfront som är mer sfärisk. Ju mindre vi gör öppningen desto mer sfärisk kommer vågfronten att se ut efter öppningen.

#+DOWNLOADED: screenshot @ 2022-02-23 19:50:38
[[file:Gemensamma_egenskaper_för_all_vågrörelse/2022-02-23_19-50-38_screenshot.png]]


Man kan se lite bättre hur det fungerar i simulationen nedan från [[https://simphy.com/][Simpy]]. Prova både sfäriska och plana vågor genom att kryssa i eller kryssa ur checkrutan.

#+begin_export html
<style>
    #simpy { width:1300px; height:800px; }
</style>

<center><iframe id="simpy" src="https://simphy.com/other-apps/huygen-principle/" style="border: 2px solid black;"></iframe></center>
#+end_export

