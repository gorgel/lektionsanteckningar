:PROPERTIES:
:ID:       1beda58a-51e0-416d-99e3-ae242d9f3b19
:END:
#+title: Prog1 tester 
#+LANGUAGE: sv
#+filetags:  
-----
- taggar ::
-----

* Systematiska tester i Python

I förra avsnittet om [[id:f6e45ceb-42c2-47e9-a120-69ce51181216][Prog1 Villkorssatser och booleska värden]] så färdigställde vi funktionen som beräknade fraktkostnaderna för vår beställning av pennor. Funktionen såg då ut som

#+begin_src python :exports both :results output
def add_shipping(order_amount : float) -> float:
  """
  add shipping costs to order amount

  given 200, expects 200 + 40 = 240
  given 135.5, expects 135.5 + 40 =  175.5
  given 400, expects 400 + 80 =  480
  given 200.01, expects 200.01 + 80 =  280.01
  given 300, expects 300 + 80 = 380
  given 320, expects 320 +120 = 440
  """
  if order_amount <= 200:
    order_amount = order_amount + 40
  elif (order_amount > 200) and (order_amount <= 300):
    order_amount = order_amount + 80
  else:
    order_amount = order_amount + 120

  return order_amount

# Vi testar funktionen
print(add_shipping(200))
print(add_shipping(135.5))
print(add_shipping(400))
print(add_shipping(200.01))
print(add_shipping(300))
print(add_shipping(320))
#+end_src

I dokumentationssträngen hade vi då exempel på vad funktionen ska ge för output givet en viss input. Sen testade vi dessa manuellt genom att anropa funktionen för dessa input och kontrollera att vi fått det resultat vi förväntat.

Nu vill vi automatisera dessa tester så vi slipper göra detta manuellt (det finns fler fördelar med detta som vi kommer märka längre fram).

I python kan man göra detta på många olika sätt, men kanske det vanligaste sättet är att använda ett framework för enhetstestning, vilket i programmering är ett verktyg oftast i form av ett bibliotek utformad för att underlätta processen att skriva, utföra och hantera enhetstester. Ett enhetstest är en typ av test där man testar enskilda delar av koden, som tex en funktion.

För att kunna använda oss av detta framework måste vi först lära oss mer om moduler i Python.

* Moduler (Bibliotek eller paket)

En *modul* eller *bibliotek* i Python är i praktiken bara en fil med python-kod. När man skapar lite större program blir det snabbt svårhanterat när man har all kod (kanske tio-tusentals rader) i en fil. Det blir svårt att få en överblick av koden och det kan var svårt att hitta till en specifik funktion. Det kan ofta vara en bra ide att bryta ner sitt program i delar som har olika funktioner. Om man programmerar till exempel ett spel kanske man vill ha en fil där huvudkoden för spelet finns, en annan fil där funktioner för kollisionsdetektering finns och en tredje som kanske innehåller olika funktioner för att rita grafik osv.

En annan fördel med att jobba med moduler är att man kan återanvända kod, som man redan skrivit, i andra projekt.

Genom att använda moduler så skapar vi också egna namnområden vilket minskar risken för att vi skriver över en funktion eller variabelnamn.

I [[id:56d4ddc0-fe5f-4cba-ac01-6daa898d3d06][Prog1 funktioner]] skapade vi en funktion som som skrev ut ett brevs olika delar

#+begin_src python :exports both :results output :session fun

# main-funktionen
def letter(first, last, signature_name):

    # Funktionskomposition med hjälpfunktionerna
    letter_content = (
        opening(first) +
        "\n\n" +
        body(first, last) +
        "\n\n" +
        closing(signature_name)
        )
    
    return letter_content

# hjälpfunktion 1
def opening(first):
    return "Kära " + \
           first + \
           ","

# hjälpfunktion 2
def body(first, last):
    body_content = (
        "Vi har upptäckt att personer med " + "\n" +
        "efternamnet " + last + " har vunnit på vårt lotteri. Så, " + "\n" +
        first + ", " + "skynda dig att hämta ut ditt pris!"
        )
    
    return body_content

# hjälpfunktion 3
def closing(signature_name):
    return (
        "Med vänliga hälsningar," +
        "\n\n" +
        signature_name +
        "\n"
        )

# exempel 1 på funktionskomposition
print(letter("Simon", "Karlsson", "Lotto Olle"))


# exempel 2 på funktionskomposition

# skapar parametrar till funktionen
first_name = "Kurt"
last_name = "Ohlsson"
signature = "Lotto Olle"

# sparar resultatet i en variabel som sedan används som input till print
result = letter(first_name, last_name, signature)
print(result)
#+end_src

Vi ska nu demonstrera hur moduler fungerar genom att bryta ner det här programmet i mindre bitar. Vi ska skapa tre filer *beginning.py*, *middle.py* och *end.py* som vi använder för att dela upp programmet i.

I filen *beginning.py* vill vi ha alla funktioner som bidrar till brevets inledning, så där lägger vi koden:

#+begin_src python :exports both :results output

def opening(first):
    return "Kära " + \
           first + \
           ","

#+end_src

I filen *middle.py* vill vi ha alla funktioner som bidrar till brevets huvuddel, så där lägger vi koden:

#+begin_src python :exports both :results output

def body(first, last):
    body_content = (
        "Vi har upptäckt att personer med " + "\n" +
        "efternamnet " + last + " har vunnit på vårt lotteri. Så, " + "\n" +
        first + ", " + "skynda dig att hämta ut ditt pris!"
        )
    
    return body_content

#+end_src

I filen *end.py* vill vi alla funktioner som bidrar till brevets avslutning, så där lägger vi koden:

#+begin_src python :exports both :results output

def closing(signature_name):
    return (
        "Med vänliga hälsningar," +
        "\n\n" +
        signature_name +
        "\n"
        )

#+end_src

Vi lägger all filerna i samma mapp och sedan skapar vi en ny fil för vårt program som vi kallar *brev.py*

TODO SKAPA NYA BILDER
#+DOWNLOADED: screenshot @ 2022-11-10 17:02:36
[[file:Moduler_(Bibliotek)/2022-11-10_17-02-36_screenshot.png]]

För Chromebook och repl.it så kan vi skapa filer i "samma mapp" enligt figuren nedan

#+DOWNLOADED: screenshot @ 2022-11-10 20:44:50
[[file:Moduler_(Bibliotek_eller_paket)/2022-11-10_20-44-50_screenshot.png]]

#+DOWNLOADED: screenshot @ 2022-11-10 20:47:47
[[file:Moduler_(Bibliotek_eller_paket)/2022-11-10_20-47-47_screenshot.png]]

#+begin_src python :exports both :results output :session fun

# main-funktionen
def letter(first, last, signature_name):

    # Funktionskomposition med hjälpfunktionerna
    letter_content = (
        opening(first) +
        "\n\n" +
        body(first, last) +
        "\n\n" +
        closing(signature_name)
        )
    
    return letter_content

# exempel 1 på funktionskomposition
print(letter("Simon", "Karlsson", "Lotto Olle"))


# exempel 2 på funktionskomposition

# skapar parametrar till funktionen
first_name = "Kurt"
last_name = "Ohlsson"
signature = "Lotto Olle"

# sparar resultatet i en variabel som sedan används som input till print
result = letter(first_name, last_name, signature)
print(result)
#+end_src

Om vi provar att köra koden nu så kommer vi få ett felmeddelande eftersom vi inte har importerat filerna (modulerna) och ändrat funktionsnamnen, så att vi talar om för Python att vi vill att den ska leta efter funktionerna i de nya modulerna.

#+begin_src python :exports both :results output
import beginning
import middle
import end

# main-funktionen
def letter(first, last, signature_name):

    # Funktionskomposition med hjälpfunktionerna
    letter_content = (
        beginning.opening(first) + # notera punktnotationen
        "\n\n" +
        middle.body(first, last) +
        "\n\n" +
        end.closing(signature_name)
        )
    
    return letter_content

# exempel 1 på funktionskomposition
print(letter("Simon", "Karlsson", "Lotto Olle"))


# exempel 2 på funktionskomposition

# skapar parametrar till funktionen
first_name = "Kurt"
last_name = "Ohlsson"
signature = "Lotto Olle"

# sparar resultatet i en variabel som sedan används som input till print
result = letter(first_name, last_name, signature)
print(result)

#+end_src
  
Kör vi nu så kommer programmet att fungera som vi tänkt oss.

Vad är det egentligen som händer när vi använder import?

om vi skriver 

#+begin_src python :exports both :results output
import beginning
#+end_src

Så kommer Python att söka efter en modul med namnet beginning mha en lista av filsystemsmappar:

- mappen varifrån filen körs
- listan av mappar i "systemvariabeln" PYTHONPATH
- en installationsberoende lista med mappar som skapades vid installationsstart

Vi kan se denna lista genom att skriva koden

#+begin_src python :exports both :results output
import sys
print(sys.path)
#+end_src

#+RESULTS:
: ['c:\\users\\skrot\\mu_code', 'C:\\Users\\skrot\\AppData\\Local\\Programs\\Mu Editor\\Python\\python38.zip', 'C:\\Users\\skrot\\AppData\\Local\\Programs\\Mu Editor\\Python\\DLLs', 'C:\\Users\\skrot\\AppData\\Local\\Programs\\Mu Editor\\Python\\lib', 'C:\\Users\\skrot\\AppData\\Local\\Programs\\Mu Editor\\Python', 'C:\\Users\\skrot\\AppData\\Local\\python\\mu\\mu_venv-38-20221110-164953', 'C:\\Users\\skrot\\AppData\\Local\\python\\mu\\mu_venv-38-20221110-164953\\lib\\site-packages', 'C:\\Users\\skrot\\AppData\\Local\\python\\mu\\mu_venv-38-20221110-164953\\lib\\site-packages\\win32', 'C:\\Users\\skrot\\AppData\\Local\\python\\mu\\mu_venv-38-20221110-164953\\lib\\site-packages\\win32\\lib', 'C:\\Users\\skrot\\AppData\\Local\\python\\mu\\mu_venv-38-20221110-164953\\lib\\site-packages\\Pythonwin']

Här ser vi att sökvägen till mappen som filen ligger i är *c:\\users\\skrot\\mu_code*

** Import

Det finns olika sätt att importera moduler in i sitt program.

Den enklaste har vi sett och är att man importerar hela modulen genom att skriva *import modulnam*

#+begin_src python :exports both :results output
import beginning
print(dir(beginning))
#+end_src

#+RESULTS:
: ['__builtins__', '__cached__', '__doc__', '__file__', '__loader__', '__name__', '__package__', '__spec__', 'opening']

Där vi kan se att vår funktion *open* nu är importerad. Vi kan bara komma åt funktionen genom att anropa den genom namnområdet beginning som skapats för modulen. Vi skriver alltså *modulnamnet.funktionsnamnet*

#+begin_src python :exports both :results output
import begining
beginning.opening("Simon")
#+end_src

#+RESULTS:
: 'Kära Simon,'

Man säger att vi anropar funktioner från den importerade modulen med punktnotation.

Man kan importera flera moduler samtidigt genom att separera dem med ett komma.

#+begin_src python :exports both :results output
import beginning, middle
print(dir(beginning))
print(dir(middle))
#+end_src

#+RESULTS:
: ['__builtins__', '__cached__', '__doc__', '__file__', '__loader__', '__name__', '__package__', '__spec__', 'opening']
: ['__builtins__', '__cached__', '__doc__', '__file__', '__loader__', '__name__', '__package__', '__spec__', 'body']

Det går också att importera en enskild funktion från en modul direkt in i huvud-namnområdet. Då skriver vi *from modulnamn import funktionsnamn* 

#+begin_src python :exports both :results output
from beginning import opening
opening("Simon")
#+end_src

Vi behöver då inte använda punktnotation för att anropa funktionen eftersom funktionen då inte är importerad in i modulens namnområde. När man importerar på detta vis får man vara försiktig så man inte skriver över någon funktion som har samma namn eftersom de nu kommer ligga i samma namnområde.

Om man vill kan man importera allt från en modul in i samma namnområde. Det gör vi med *from modulnamn import **

#+begin_src python :exports both :results output
from beginning import *
#+end_src

Detta använder man mest till att snabbt prova en moduls funktioner i interpreteraren.

Man kan också importera en modul och samtidigt referera till modulen med ett annat namn. Det gör vi med *import modulnamn as annat_namn*

#+begin_src python :exports both :results output
import beginning as beg
beg.opening("Simon")
#+end_src

** Paket (Packages)

Python är ett språk med ett rikt ekosystem av moduler som är skrivna av andra programmerare. Dessa kan man använda sig av i sina egna program. De är samlade i en databas som kallas *Pypi*, Python package index. Här är en länk till denna:

[[https://pypi.org/][PyPi]]

Vid skrivande stund finns det 414,373 st paket. Ett paket i python är bara en samling moduler i en hierarkisk filstruktur. Det kan vara en modul eller flera som tillsammans utgör en större modul, då kallat paket. Vi använder dem på i stort sätt samma sätt som vanliga moduler som vi precis sett. Men för att göra dem tillgängliga måste vi ladda ner dem.

Ett sätt att få tag i dem är att helt enkelt ladda ner dem och spara dem i den mapp man behöver använda dem i, för att sedan importera dem med import.

Ett annat, mer vanlig sätt, sätt är att använda en pakethanterare som *pip* (pip install packages). pip underlättar för oss att hämta och använda andras moduler som är publicerade på Pypi.

** pip

Pip brukar automatiskt installeras tillsammans med Python om man inte aktivt väljer att plocka bort det från installationen.

Beroende på hur man använder Python kan det finnas olika sätt att använda och installera moduler. Vi börjar med det vanligaste sättet på Windows.

** Pip på Windows

För att se om pip är installerat på Windows så öppnar vi kommandotolken genom att söka och skriva cmd i sökfältet

#+DOWNLOADED: screenshot @ 2022-11-10 19:00:52
[[file:Packages_(Paket)/2022-11-10_19-00-52_screenshot.png]]

I kommandotolken skriver vi *pip* och trycker enter

#+DOWNLOADED: screenshot @ 2022-11-10 19:02:19
[[file:Packages_(Paket)/2022-11-10_19-02-19_screenshot.png]]

Om pip är installerat på rätt sätt så kommer vi få fram en massa text som talar om lite hur vi kan använda pip.

I annat fall, om det inte skulle fungera, så brukar man få följande text tillbaka:

: 'pip' is not recognized as an internal or external command,
: operable program or batch file.

Det betyder att pip inte har lagts till på rätt sätt så att windows kan hitta pip. För att fixa det behöver vi köra python installern igen och följ stegen nedan.

#+DOWNLOADED: screenshot @ 2022-11-10 19:07:18
[[file:Packages_(Paket)/2022-11-10_19-07-18_screenshot.png]]

#+DOWNLOADED: screenshot @ 2022-11-10 19:11:36
[[file:Packages_(Paket)/2022-11-10_19-11-36_screenshot.png]]

#+DOWNLOADED: screenshot @ 2022-11-10 19:12:15
[[file:Packages_(Paket)/2022-11-10_19-12-15_screenshot.png]]

#+DOWNLOADED: screenshot @ 2022-11-10 19:13:36
[[file:Packages_(Paket)/2022-11-10_19-13-36_screenshot.png]]

Öppna sedan kommandotolken igen och skriv pip. Nu borde pip fungera.

för att installera ett paket skriver vi *pip install paketnamn*. Vi kan nu prova att installera ett program som heter matplotlib

#+DOWNLOADED: screenshot @ 2022-11-10 19:17:30
[[file:Packages_(Paket)/2022-11-10_19-17-30_screenshot.png]]

När vi installerar paketet matplotlib kommer pip sköta nedladdningen av matplotlib och alla andra moduler som matplotlib behöver för att fungera.

I windows kommer paket som installeras med pip att hamna (i normala fall) i mappen

: C:\Users\skrot\AppData\Local\Programs\Python\Python311\Lib\site-packages

De inbyggda modulerna i Python finns i mappen

: C:\Users\skrot\AppData\Local\Programs\Python\Python311\Lib

Några vanliga pip kommandon (en större lista kan ses om man bara skriver pip i kommandotolken)

- pip uninstall paketnamn (avinstallera paket)
- pip list (lista alla installerade paket)

** pip på Chromebook

För att installera paket med pip i repl.it så får vi trycka på *Shell-fliken*

#+DOWNLOADED: screenshot @ 2022-11-10 20:01:37
[[file:Packages_(Paket)/2022-11-10_20-01-37_screenshot.png]]

kontrollera att pip är installerat (Ska vara det automatiskt i repl.it) genom att skriva pip och trycka enter.

#+DOWNLOADED: screenshot @ 2022-11-10 20:16:45
[[file:Packages_(Paket)/2022-11-10_20-16-45_screenshot.png]]

Installera matplotlib med *pip install matplotlib*

#+DOWNLOADED: screenshot @ 2022-11-10 20:18:38
[[file:Packages_(Paket)/2022-11-10_20-18-38_screenshot.png]]

** pip i Thonny

Thonny har en egen version av Python installerat i en så kallad *virtuell miljö* som är separat från Windows egen pythoninstallation. Därför måste vi installera moduler i Thonny på ett lite annorlunda sätt. 


#+DOWNLOADED: screenshot @ 2024-03-20 10:07:22
#+ATTR_HTML: :width 850px
[[file:Paket_(Packages)/2024-03-20_10-07-22_screenshot.png]]



#+DOWNLOADED: screenshot @ 2024-03-20 10:09:05
#+ATTR_HTML: :width 850px
[[file:Paket_(Packages)/2024-03-20_10-09-05_screenshot.png]]

Skriv in i rutan det paket du vill installera. Nu kan vi prova att ladda ner modulen för enhetstest som vi ska använda. Skriv in *pytest* och klicka på "Search on PyPI"-knappen.

#+DOWNLOADED: screenshot @ 2024-03-20 10:12:42
#+ATTR_HTML: :width 850px
[[file:Paket_(Packages)/2024-03-20_10-12-42_screenshot.png]]

#+DOWNLOADED: screenshot @ 2024-03-20 10:13:56
#+ATTR_HTML: :width 850px
[[file:Paket_(Packages)/2024-03-20_10-13-56_screenshot.png]]

#+DOWNLOADED: screenshot @ 2024-03-20 10:15:19
#+ATTR_HTML: :width 850px
[[file:Paket_(Packages)/2024-03-20_10-15-19_screenshot.png]]


** Övning (valfri)

- Installera de två paketen *matplotlib* och *numpy* med pip

- Genom att söka på internet, ta reda på grunderna för hur man gör en enkel linjeplot i matplotlib samt numpy, och återskapa så gott som möjligt följande figur (sinusfunktion). 

  #+DOWNLOADED: screenshot @ 2022-11-10 19:50:34
  [[file:Övningar/2022-11-10_19-50-34_screenshot.png]]

Se om du kan snygga till grafen som i figuren nedan

 #+DOWNLOADED: screenshot @ 2022-11-11 10:01:30
 [[file:Övningar/2022-11-11_10-01-30_screenshot.png]]


- Googla efter populära paket i Python och se om det finns något som intresserar dig. 

* Åter till systematiska tester i Python

Vi ska nu automatisera testerna i funktionen nedan

#+begin_src python :exports both :results output
def add_shipping(order_amount : float) -> float:
  """
  add shipping costs to order amount

  given 200, expects 200 + 40 = 240
  given 135.5, expects 135.5 + 40 =  175.5
  given 400, expects 400 + 80 =  480
  given 200.01, expects 200.01 + 80 =  280.01
  given 300, expects 300 + 80 = 380
  given 320, expects 320 +120 = 440
  """
  if order_amount <= 200:
    order_amount = order_amount + 40
  elif (order_amount > 200) and (order_amount <= 300):
    order_amount = order_amount + 80
  else:
    order_amount = order_amount + 120

  return order_amount

# Vi testar funktionen
print(add_shipping(200))
print(add_shipping(135.5))
print(add_shipping(400))
print(add_shipping(200.01))
print(add_shipping(300))
print(add_shipping(320))
#+end_src

Det kan vi göra genom att först importera modulen pytest

#+begin_src python :exports both :results output
import pytest

def add_shipping(order_amount : float) -> float:
  """
  add shipping costs to order amount

  given 200, expects 200 + 40 = 240
  given 135.5, expects 135.5 + 40 =  175.5
  given 400, expects 400 + 80 =  480
  given 200.01, expects 200.01 + 80 =  280.01
  given 300, expects 300 + 80 = 380
  given 320, expects 320 +120 = 440
  """
  if order_amount <= 200:
    order_amount = order_amount + 40
  elif (order_amount > 200) and (order_amount <= 300):
    order_amount = order_amount + 80
  else:
    order_amount = order_amount + 120

  return order_amount

# Vi testar funktionen
print(add_shipping(200))
print(add_shipping(135.5))
print(add_shipping(400))
print(add_shipping(200.01))
print(add_shipping(300))
print(add_shipping(320))
#+end_src

Sen måste vi skapa en ny funktion som ska testa den funktion vi vill testa. Vi namnger denna testfunktion till *test​_funktionsnamn* och placera testfunktionen över funktionen som ska testas

#+begin_src python :exports both :results output
import pytest

# Testfunction
def test_add_shipping():

def add_shipping(order_amount : float) -> float:
  """
  add shipping costs to order amount

  given 200, expects 200 + 40 = 240
  given 135.5, expects 135.5 + 40 =  175.5
  given 400, expects 400 + 80 =  480
  given 200.01, expects 200.01 + 80 =  280.01
  given 300, expects 300 + 80 = 380
  given 320, expects 320 +120 = 440
  """
  if order_amount <= 200:
    order_amount = order_amount + 40
  elif (order_amount > 200) and (order_amount <= 300):
    order_amount = order_amount + 80
  else:
    order_amount = order_amount + 120

  return order_amount

  
#+end_src

Sen lägger vi till testen genom *assert* som nedan

#+begin_src python :exports both :results output
import pytest

# Testfunction
def test_add_shipping():
  assert add_shipping(200) == 240
  assert add_shipping(135.5) == 175.5
  assert add_shipping(400) == 480
  assert add_shipping(200.01) == 280.01
  assert add_shipping(300) == 380
  assert add_shipping(320) == 440

def add_shipping(order_amount : float) -> float:
  """
  add shipping costs to order amount

  given 200, expects 200 + 40 = 240
  given 135.5, expects 135.5 + 40 =  175.5
  given 400, expects 400 + 80 =  480
  given 200.01, expects 200.01 + 80 =  280.01
  given 300, expects 300 + 80 = 380
  given 320, expects 320 +120 = 440
  """
  if order_amount <= 200:
    order_amount = order_amount + 40
  elif (order_amount > 200) and (order_amount <= 300):
    order_amount = order_amount + 80
  else:
    order_amount = order_amount + 120

  return order_amount

#+end_src

Till sist kör vi testen genom att anropa funktionen i modulen pytest pytest.main(["filnamnet"]) sist i programmet

#+begin_src python :exports both :results output
import pytest

# Testfunction
def test_add_shipping():
  assert add_shipping(200) == 240
  assert add_shipping(135.5) == 175.5
  assert add_shipping(400) == 480
  assert add_shipping(200.01) == 280.01
  assert add_shipping(300) == 380
  assert add_shipping(320) == 440

def add_shipping(order_amount : float) -> float:
  """
  add shipping costs to order amount

  given 200, expects 200 + 40 = 240
  given 135.5, expects 135.5 + 40 =  175.5
  given 400, expects 400 + 80 =  480
  given 200.01, expects 200.01 + 80 =  280.01
  given 300, expects 300 + 80 = 380
  given 320, expects 320 +120 = 440
  """
  if order_amount <= 200:
    order_amount = order_amount + 40
  elif (order_amount > 200) and (order_amount <= 300):
    order_amount = order_amount + 80
  else:
    order_amount = order_amount + 120

  return order_amount

pytest.main(["shipping.py"])
#+end_src

Kör vi denna så kommer vi få lite information i REPLn angående vårt test. I det här fallet ser vi att ett av testen "failar"

#+DOWNLOADED: screenshot @ 2024-03-20 11:02:38
#+ATTR_HTML: :width 850px
[[file:Åter_till_systematiska_tester_i_Python/2024-03-20_11-02-38_screenshot.png]]

#+DOWNLOADED: screenshot @ 2024-03-20 11:04:04
#+ATTR_HTML: :width 850px
[[file:Åter_till_systematiska_tester_i_Python/2024-03-20_11-04-04_screenshot.png]]

Då kan vi snygga till denna funktion genom att ta bort "testinformationen" i dokumentationssträngen och korrigera testfunktionen genom att ta bort testet för input 400, eller alternativt korrigera det så det blir rätt, dvs == 520. Jag väljer att ta bort testet då vi redan testar det med testet som har input 320. Vi får då

#+begin_src python :exports both :results output
import pytest

# Testfunction
def test_add_shipping():
  assert add_shipping(200) == 240
  assert add_shipping(135.5) == 175.5
  assert add_shipping(200.01) == 280.01
  assert add_shipping(300) == 380
  assert add_shipping(320) == 440

def add_shipping(order_amount : float) -> float:
  """
  add shipping costs to order amount
  """
  if order_amount <= 200:
    order_amount = order_amount + 40
  elif (order_amount > 200) and (order_amount <= 300):
    order_amount = order_amount + 80
  else:
    order_amount = order_amount + 120

  return order_amount

pytest.main(["shipping.py"])
#+end_src

* Övning (inlämning)

- Färdigställ programmet från övningen i [[id:56d4ddc0-fe5f-4cba-ac01-6daa898d3d06][Prog1 funktioner]] där programmet ska beräknar totala kostnaden av att beställa ett visst antal graverade pennor, inklusive frakt. En penna kostar 70 kr per styck och sedan kostar det 2 kr per graverad karaktär på pennan. Space räknas också som en karaktär. Om orderkostnaden understiger 200 kr så kostar frakten 40 kr. Om orderkostnaden ligger melllan 200 och 300 kr blir fraktkostnaden 80 kr och om orderkostnaden är över 300 kr så kostar frakten 120 kr.

Här är en mall som man kan utgå ifrån

#+begin_src python :exports both :results output

def pen_order():

def add_shipping():

def total_pen_cost():

#+end_src

Skriv typannoteringar, dokumentationssträngar och tester till de tre funktionerna.

