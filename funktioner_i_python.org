:PROPERTIES:
:ID:       f358b6bf-71f6-4f85-bf52-365af07d03f3
:END:
#+title: Funktioner i Python 
#+LANGUAGE: sv
#+filetags:  
-----
- taggar ::
-----

* Funktioner
:PROPERTIES:
:ID:       cec67f40-6ba5-4182-a854-7c962e0c1fe7
:END:

Ofta när man programmerar vill man köra en viss bit kod flera gånger. Istället för att skriva/kopiera koden upprepade gånger där den behövs så kan man _definera_ en _funktion_. En funktion är enkelt sätt en namngivning av ett visst block med kod. Genom att _anropa_ funktionens namn så kör vi kodblocket.

#+begin_src python :exports both :results output
def hello():
    """Skriver ut Hello"""
    print("Hello")
    print("My name is")
    print("Simon")

hello() # kör funktionen genom att anropa den
#+end_src

#+RESULTS:
: Hello
: My name is
: Simon

Vi definerar en funktion med nyckelordet *def* följt av funktionsnamnet, ett parentespar och avslutas med ett kolon. Sedan skriver vi koden (indragen) som ska köras när vi anropar funktionen. För att köra funktionen skriver vi funktionens namn följt av parenteser precis på samma sätt som vi har använt de inbyggda funktionerna i Python.

Vi har sett att de inbyggda funktionerna även kan ta _argument_, dvs medskickad data som funktionen på något sätt ska behandlas. Ett exempel kan vara att omvandla från grader Kelvin till Celcius. Ett program som vi tidigare sett, men då behövt ändra varaibeln varje gång vi kört programmet. Om vi gör programmet till en funktion kan vi återanvända den när vi behöver den och dessutom kan vi ge temperaturen som vi ska omvandla som argument så funktionen blir mer flexibel.

#+begin_src python :exports both :results output
def kelvin2celcius(temp_kelvin):
    temp_celcius = temp_kelvin-273
    print(temp_celcius, "grader Celcius")
    
kelvin2celcius(100)
kelvin2celcius(500)
kelvin2celcius(5000)
#+end_src

#+RESULTS:
: -173 grader Celcius
: 227 grader Celcius
: 4727 grader Celcius

Här kommer _parametern_ (egentligen bara en variabel, men kallas i _funktionshuvudet_ för parameter. Värdet som den får kallas för argument) att ta emot ett argument som den sedan omvandlar i funktionsblocket till grader Kelvin och printar.

* Scope

Parametern som finns i funktionsdefinitionen samt variabeln *temp_celcius* är *lokala*. Det innebär att de inte är åtkomliga utifrån funktionen. Alltså kommer variabler med samma namn utanför och innanför funktionen vara helt skilda objekt.

#+begin_src python :exports both :results output

def kelvin2celcius(temp_kelvin):
    temp_celcius = temp_kelvin-273
    print("i funktionen är temp_celcius",temp_celcius)
    
temp_celcius = 300
print("utanför funktionen är temp_celcius", temp_celcius)
kelvin2celcius(800)    
print("utanför funktionen är temp_celcius fortfarande", temp_celcius)
#+end_src

#+RESULTS:
: utanför funktionen är temp_celcius 300
: i funktionen är temp_celcius 527
: utanför funktionen är temp_celcius fortfarande 300

variabler som skrivs utanför funktioner kallas för _globala variabler_. Dessa kan man inte ändra hur som helst innuti funktioner. Däremot kan vi läsa globala variablers värden och tilldela dessa till en ny variabel som är lokal i funktionen.

#+begin_src python :exports both :results output

a = 10
b = 20

def scope():
    b = a
    print(b)

print(b)
scope()
print(b)
#+end_src

#+RESULTS:
: 20
: 10
: 20

Allmänt vill man undvika att använda globala variabler innuti funktioner, men i större program kan det finnas tillfällen då man behöver ändra på en global variabel innuti en funktion. Då kan man använda nyckelordet *global*.

#+begin_src python :exports both :results output
a = 10
b = 20

def scope():
    global b
    print(b)

print(b)
scope()
print(b)
#+end_src

#+RESULTS:
: 20
: 20
: 20

Ibland kan man behöva _returnera_ något som man gjort i en funktion

#+begin_src python :exports both :results output
def medelvärde(a,b,c,d):
    summa = a + b + c + d
    medelvärde = summa/4
    return medelvärde

print("medelvärdet av 1,2,3,4 är", medelvärde(1,2,3,4))
#+end_src

#+RESULTS:
: medelvärdet av 1,2,3,4 är 2.5

Det går också att fånga upp ett returnerat värde i en variabel, för att sedan manipulera det vidare på något sätt

#+begin_src python :exports both :results output
def medelvärde(a,b,c,d):
    summa = a + b + c + d
    medelvärde = summa/4
    return medelvärde

medel = medelvärde(4,5,6,7)
print(medel)
medel = medel +5
print(medel)

#+end_src

#+RESULTS:
: 5.5
: 10.5

Ibland vill man ha vissa standardvärden på parametrar, men med en möjlighet att ändra dessa.

#+begin_src python :exports both :results output
def hej(namn, meddelande="kul att se dig!"):
    print("Hej", namn, meddelande)

hej("Simon")
hej("Simon", "hur är läget?")
#+end_src

#+RESULTS:
: Hej Simon kul att se dig!
: Hej Simon hur är läget?

* Programstrukturer

Förutom att man kan skapa återanvändbara sekvenser av kod om man skriver funktioner så är en annan fördel att man kan enklare kan strukturera sina program. Detta brukar göra programmen mer överskådliga och läsbara.

#+begin_src python :exports both :results output
import turtle

def eye(size):
    turtle.seth(0)
    turtle.circle(size)
    turtle.circle(size/3)
    
def mouth(length):
    turtle.seth(0)
    turtle.forward(length)
    turtle.backward(length)
    turtle.left(270)
    turtle.circle(length/2,180)

def head(size):
    base = 20
    turtle.goto(0,0)
    turtle.forward(base*size)
    turtle.left(90)
    turtle.forward((base*size)/2)
    turtle.right(90)
    turtle.circle((base/5)*size, 180)
    turtle.right(90)
    turtle.forward((base*size)/2)
    turtle.left(90)
    turtle.forward(base*size)
    turtle.left(90)
    turtle.forward((base*size)/2)
    turtle.right(90)
    turtle.circle((base/5)*size, 180)
    turtle.right(90)
    turtle.forward((base*size)/2)


turtle.speed(6)
head(10)
turtle.penup()
turtle.goto(70, 160)
turtle.pendown()
eye(10)
turtle.penup()
turtle.goto(130,160)
turtle.pendown()
eye(10)
turtle.penup()
turtle.goto(50,90)
turtle.pendown()
mouth(100)
#+end_src

#+RESULTS:


* Övningar

- Skriv en funktion som returnerar det största av två tal, vilka skickas som parametrar

  
- skriv en funktion "nettopris" som tar ett visst bruttobelopp som parameter och returnerar ett rabbaterat pris. Om beloppet är under 500kr ges ingen rabbat. Om beloppet är mer än 500kr, men under 1000kr ges en rabbat på 2%. Är beloppet mer än 1000 ges 5%.


- Rita en valfri figur i turtle, men bryt ner programmet i mindre funktioner.
