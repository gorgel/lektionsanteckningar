:PROPERTIES:
:ID:       261dac5c-7283-43ba-85cd-6b1ba57c93c6
:END:
#+title: Friktion 
#+LANGUAGE: sv
#+filetags:  
-----
- taggar ::
-----

* Friktionskraft

Hur uppstår friktionskraft?

Säg att vi har två föremål. Kollar vi riktigt nära på dessa föremål så kan vi se att föremålens ytor inte är helt släta. Det finns vissa ojämnheter. Dessa är överdrivna i bilden nedan.

#+DOWNLOADED: screenshot @ 2022-09-28 18:16:48
[[file:Friktionskraft/2022-09-28_18-16-48_screenshot.png]]

När ytorna kommer i kontakt och och ytorna rör sig relativt varandra kommer dessa ojämnheter "haka i varandra". Säg att det övre föremålet rör sig till vänster som i bilden nedan

#+DOWNLOADED: screenshot @ 2022-09-28 18:19:03
[[file:Friktionskraft/2022-09-28_18-19-03_screenshot.png]]

Då kommer det övre föremålet att ge en kraft till vänster på det undre föremålet på de ställen där kontakt sker. Enligt Newtons tredje lag kommer då det undre föremålet att påverka det övre med en lika stor kraft men åt höger. Summan av alla dessa små krafter som verka på det övre blocket är det som vi kallar för _friktionskraften_.

Friktionskraften är alltså _alltid motsatt rörelseriktningen_.

** Vilofriktion och glidfriktion :noexport:

När vi knuffar ett föremål så kommer friktionskraften att till en början vara lika stor som den kraft vi knuffar med och föremålet står stilla. Denna friktionskraft kallar vi då för _vilofriktion_. Men upp till en viss gräns så kommer inte friktionskraften bli större och den kraft vi knuffar med kommer då att bli större än friktionskraften så att föremålet får en resultantkraft i knuffriktningen och börjar röra sig. I det ögonblicket kommer friktionskraften att bli ännu mindre än vilofriktionen och kommer stabilisera sig på ett lite lägre värde. Denna friktionskraft kallar vi då för _glidfriktion_. Vi kan illustrera detta med grafen och figurerna nedan.

#+DOWNLOADED: screenshot @ 2022-09-28 18:27:35
[[file:Vilofriktion_och_glidfriktion/2022-09-28_18-27-35_screenshot.png]]

Vi kommer i stort sätt alltid jobba med glidfriktionen i olika uppgifter.

*** Testa själv

För att "känna" att det verkligen blir vilofriktion och glidfriktion så kan man prova att knuffa en bänk.

** Exempel

En kista släpas längs med marken med konstant hastighet. 
Rita kistan och sätt ut alla krafter som verkar på kistan.  

#+DOWNLOADED: screenshot @ 2022-09-28 18:44:43
[[file:Friktionskraft/2022-09-28_18-44-43_screenshot.png]]

** Friktionskraft

Storleken på friktionskraften beror av två saker:

1. Hur stor kraft föremålet utövar mot underlaget
2. Materialen som föremålet och underlaget har mot varandra
   
Vi sammanfattar detta med ekvationen

$F_f = \mu \cdot F_N$

där $F_f$ är friktionskraften, $F_N$ är normalkraften och \mu är friktionstalet

** Friktionstal

Olika material ger upphov till olika mycket friktion. Detta beskrivs i friktionstalet μ. (uttalas “my”)

Material som ger mycket friktion har ett högre friktionstal. Se formelboken s.9

Friktionstalet motsvarar kvoten:

$\mu = \frac{F_f}{F_N}$

Vad har friktionstalet för enhet?

** Exempel

Simon bestämmer friktionstalet för en pulka. Med en dynamometer drar han pulkan framåt med konstant hastighet över snö. Dynamometern hålls parallellt med marken och visar 1,2 N. Pulkan väger 1,3 kg. 

#+DOWNLOADED: screenshot @ 2022-09-29 08:05:37
[[file:Friktionskraft/2022-09-29_08-05-37_screenshot.png]]


a) Hur stor är normalkraften som verkar på pulkan?

b) Vilket friktionstal har pulkan mot snön?

c) Varför ska pulkan dras med konstant hastighet?

d) Med vilken kraft måste Simon dra sin dotter Vida för att hon ska röra sig framåt när hon sitter i pulkan? Hon väger 15,4 kg. 

*** Lösning

#+DOWNLOADED: screenshot @ 2022-09-29 08:06:20
[[file:Friktionskraft/2022-09-29_08-06-20_screenshot.png]]


#+DOWNLOADED: screenshot @ 2022-09-29 08:06:51
#+ATTR_HTML: :width 800px
[[file:Friktionskraft/2022-09-29_08-06-51_screenshot.png]]

c)

Dragkraften blir då lika stor som friktionskraften


#+DOWNLOADED: screenshot @ 2022-09-29 08:07:43
[[file:Friktionskraft/2022-09-29_08-07-43_screenshot.png]]


** Övning

Calle och Lotta hjälps åt att flytta en byrå. Byrån väger 72 kg. När Calle skjuter på med 190 N och Lotta drar med 130 N så rör sig byrån med accelerationen 0,4 m/s^2. Hur stor är friktionskraften på byrån?

a) Hur stor är friktionskraften på byrån?

b) Hur stort är friktionstalet mellan golvet och byråns undersida?

*** Lösning

a)

#+DOWNLOADED: screenshot @ 2022-09-28 19:07:27
#+ATTR_HTML: :width 800px
[[file:Friktionskraft/2022-09-28_19-07-27_screenshot.png]]


b)

#+DOWNLOADED: screenshot @ 2022-09-29 08:19:28
[[file:Friktionskraft/2022-09-29_08-19-28_screenshot.png]]

** Exempel

Vi bestämmer friktionstalet mellan trä och trä.

Utrustning: Dynamometer, massa och träkloss. 


