:PROPERTIES:
:ID:       62fff368-6973-4d0d-b67d-6c52fdd26798
:END:
#+title: En sorteringsalgoritm i Python 
#+LANGUAGE: sv
#+filetags:  
-----
- taggar ::
-----

* Längden av en lista

Ibland kan det vara bra att veta hur lång en lista är. Det kan vi göra med den inbyggda funktionen *len*. Nedan har vi ett exempel på hur man använder funktionen och vi ser att för att ta reda på längden av listan ger vi helt enkelt listan som argument till len-funktionen.

#+begin_src python :exports both :results output
lista = [1, 3, 5, 6, 7]
print(len(lista))
#+end_src

#+RESULTS:
: 5

Här ser vi att exempellistan hade längden 5 eftersom den innehåller 5 element. Elementen behöver inte vara tal utan det kan vara vad som helst.

#+begin_src python :exports both :results output
lista = [2, "hello", [3,4,5], print, 7.3]
print(len(lista))
#+end_src

#+RESULTS:
: 5

Här ser vi att även fast vi har andra typer av objekt så är längden på listan 5 även fast vi tex har en nästlad lista på position 3.

I Denna lektion kommer vi att behöva denna funktion för att skapa en sortetingsalgoritm. Dvs en metod för att sortera en lista.

* Algoritmer

Vad är en algoritm?

En algoritm kan man säga är en metodbeskrivning om hur man löser ett visst problem. En algoritm består av stegvisa exakta instruktioner om hur man löser problemet. En algoritm likanar mycket det vi gör när vi programmera i tex Python, men en algoritm behöver inte defineras i ett viss språk, utan är oberoende av vilket språk den är defnerad i. Det finns ofta många sätt att lösa samma problem men olika algoritmer kan vara olika effektiva.

Ett exempel på ett problem som kan lösas med olika algoritmer är att sortera en lista med med slumpade tal, där talen ska sorteras med minsta talet först i listan, följt av större tal i ordning.

** Urvalssortering

Urvalssortering är en av flera olika algoritmer som kan användas för att sortera en sådan lista. Metoden är ganska innefektiv, men den är relativt enkel att förstå och koda i Python.

Innan man börjar skriva koda för att lösa ett sådant problem måste man ha klart för sig hur sin lösning fungerar. Man måste förstå den. Vi ska kolla på hur urvalsalgoritmen fungerar genom att kolla på ett konkret exempel.

Låt oss säga att vi har listan 8, 3, 7, 9, 5, 2 som vi ska sortera. För att sortera denna lista enligt urvalssortering så måste vi loopa över talen i listan i två led.

*** Iteration 1

I första iterationen börjar vi med att kolla på det första talet i listan.

#+DOWNLOADED: screenshot @ 2023-01-06 13:27:02
[[file:Algoritmer/2023-01-06_13-27-02_screenshot.png]]

Vi kollar om det första talet är det minsta talet i listan så långt. Det kommer det vara så vi sparar det som det minsta talet. Sedan jämför vi det med nästa tal, i det här fallet 3. Då ser vi att 3 är mindre än 8 så då sätter vi istället 3 som det minsta talet.

Sedan går vi till nästa tal i listan, dvs 7 och jämför.

#+DOWNLOADED: screenshot @ 2023-01-06 13:30:05
[[file:Algoritmer/2023-01-06_13-30-05_screenshot.png]]

Är 7 mindre än 3? Nej. Så 3 är fortfarande det minsta talet och vi går vidare till nästa tal, dvs 9

#+DOWNLOADED: screenshot @ 2023-01-06 13:31:52
[[file:Algoritmer/2023-01-06_13-31-52_screenshot.png]]

9 är inte mindre än 3 så vi går vidare till nästa tal, dvs 5

#+DOWNLOADED: screenshot @ 2023-01-06 13:34:27
[[file:Algoritmer/2023-01-06_13-34-27_screenshot.png]]

5 är inte mindre än 3 så vi går vidare till nästa tal, dvs 2

#+DOWNLOADED: screenshot @ 2023-01-06 13:35:33
[[file:Algoritmer/2023-01-06_13-35-33_screenshot.png]]

2 är mindre än 3 s vi sätter 2 som det nya minsta talet. 

#+DOWNLOADED: screenshot @ 2023-01-06 13:36:23
[[file:Algoritmer/2023-01-06_13-36-23_screenshot.png]]

När vi har hittat det minsta talet så byter vi plats på det minsta talet (2) och det talet vi började med (8).

#+DOWNLOADED: screenshot @ 2023-01-06 13:37:55
[[file:Algoritmer/2023-01-06_13-37-55_screenshot.png]]

Då kommer det första elementet i listan att vara sorterat, medans resten av listan fortfarande är osorterad.

*** Iteration 2

Då har vi loopat över det första talet i listan. Nu ska vi loopa över det andra talet i listan. Det andra talet i listan är 3 och vi gör på samma sätt som innan att sätter det första talet som det minsta talet. Sen loopar vi över resten av listan, dvs talen 7 och framåt, och kollar vilket tal som är det minsta.

#+DOWNLOADED: screenshot @ 2023-01-06 13:42:08
[[file:Algoritmer/2023-01-06_13-42-08_screenshot.png]]

Vi börjar med att kolla om 7 är mindre än 3. # är fortfarande minst så vi går till nästa tal, dvs 9

#+DOWNLOADED: screenshot @ 2023-01-06 13:43:44
[[file:Algoritmer/2023-01-06_13-43-44_screenshot.png]]

9 är större än 3, så 3 är fortfarande det minsta talet. Sen fortsätter vi att kolla på alla resterande tal i listan. Vi kommer fram till att 3 är det minsta talet, så vi byter plats på detta tal och det första, vilket i detta fall innebär att vi flyttar talet till samma plats som det fanns på.

#+DOWNLOADED: screenshot @ 2023-01-06 13:48:11
[[file:Algoritmer/2023-01-06_13-48-11_screenshot.png]]

Nu är de två första talen sorterade.

*** Iteration 3

Sen upprepar vi samma procedur för det tredje talet i listan. Vi sätter först minstatalet till 7 och loppar över listan för att hitta det minst talet.

#+DOWNLOADED: screenshot @ 2023-01-06 13:53:59
[[file:Algoritmer/2023-01-06_13-53-59_screenshot.png]]

7 är forftarande mindre än 9 så vi behåller 7 som minstatal och går till nästa, dvs 5

#+DOWNLOADED: screenshot @ 2023-01-06 13:54:59
[[file:Algoritmer/2023-01-06_13-54-59_screenshot.png]]

5 är mindre än 7 så vi sätter detta tal som det minsta talet. Vi går till nästa tal som är 8

#+DOWNLOADED: screenshot @ 2023-01-06 13:56:32
[[file:Algoritmer/2023-01-06_13-56-32_screenshot.png]]

8 är inte mindre än 5 så 5 är fortfarnde det minsta talet.

#+DOWNLOADED: screenshot @ 2023-01-06 13:57:54
[[file:Algoritmer/2023-01-06_13-57-54_screenshot.png]]

Vi byter plats på det första talet och det minsta. Då är halva listan sorterad.

*** Iteration 4

Vi upprepar sedan proceduren så vi får

#+DOWNLOADED: screenshot @ 2023-01-06 14:00:14
[[file:Algoritmer/2023-01-06_14-00-14_screenshot.png]]

*** Iteration 5

Vi upprepar proceduren igen, men nu ser vi att vi behöver bara byta plats på de två sista elementen så är listan sorterad

#+DOWNLOADED: screenshot @ 2023-01-06 14:00:33
[[file:Algoritmer/2023-01-06_14-00-33_screenshot.png]]

För en lista med 6 element behöve vi alltså bara loopa 5 "yttre" gånger, men för varje loop "inre loop behövde vi loopa först 6, sen 5, 4, 3 osv.  

*** Visualisering av algoritmen

För att bättre förstå algoritmen så kan vi visualisera hela förloppet med hjälp av en visualisering på [[https://visualgo.net/en/sorting][vusialgo.net]]. Om vi väljer algoritmen "Selection sort" (Urvalssortering på svenska), lägger in listan 8, 3, 7, 9, 5, 2 och stegar igenom processen så ser vi steg för steg hur det hela går till.

#+begin_export html
<style>
    #myFrame { width:100%; height:600px; }
</style>

<iframe id="myFrame" src="https://visualgo.net/en/sorting"  style="border: 2px solid black;"></iframe>
#+end_export

https://visualgo.net/en/sorting

Okej, så förhoppningsvis har vi nu en bättre förståelse hur algoritmen fungerar. Innan vi försöker omvandla algoritmen till Python-kod så kan det vara bra att försöka formulera algoritmen med det vi kallar för *pseudokod* 
** Pseudokod

Pseudokod är i datorprogrammering en programkods­liknande text som används för att be­skriva hur algoritmen bör vara formulerad.

När man skriver psuedokod så försöker man skriva ner vad programmet ska göra på ett sådant sätt att det liknar programmering men på ett lite mindre strikt sätt och lite mer beskrivande. Detta gör man som ett mellansteg för att det ska bli enklare att sedan skriva koden direkt i Python

För algoritmen ovan skulle man kunna skriva

#+begin_src

Loopa över talen i listan
    För första talet, sätt värdet av index 0 till det minsta värdet.
    Spara också minstatalets index i en variabel
    Loopa över talen med index 1-5
        Om värde_av_index < minstatal:
	    sätt värde_av_index som minstatal
	    Spara detta värdes index
    När loopen är klar, byt plats på värdet för index 0 till värdet
    för index av minstatal 
 
    För andra talet, sätt värdet av index 1 till det minsta värdet.
    Spara också minstatalets index i en variabel
    Loopa över talen med index 2-5
        Om värde_av_index < minstatal:
	    sätt värde_av_index som minstatal
	    Spara detta värdes index
    När loopen är klar, byt plats på värdet för index 1 till värdet
    för index av minstatal 

    För tredje talet, sätt värdet av index 2 till det minsta värdet.
    Spara också minstatalets index i en variabel
    Loopa över talen med index 3-5
        Om värde_av_index < minstatal:
	    sätt värde_av_index som minstatal
	    Spara detta värdes index
    När loopen är klar, byt plats på värdet för index 2 till värdet
    för index av minstatal 
    .
    .
    .

    För femte talet, sätt värdet av index 4 till det minsta värdet.
    Spara också minstatalets index i en variabel
    Loopa över talen med index 5-5
        Om värde_av_index < minstatal:
	    sätt värde_av_index som minstatal
	    Spara detta värdes index
    När loopen är klar, byt plats på värdet för index 4 till värdet
    för index av minstatal 
#+end_src

** Övning

1. Använd pseudokoden ovan för att skriva sorteringsalgoritmen i Python. Börja med att definera en lista med tal, tex den som vi använt ovan [8,3,7,9,5,2] som du använder som lista att sortera.

2. När du lyckats få algoritmen att fungera kan du testa att skriva om den till en funktion som tar en lista med längden n (dvs en lista med med en viss mängd element 100, 243, 34314 eller vad som helst) som argument och returnerar den sorterade listan

*** Lösning

#+begin_src python :exports both :results output

list = [8,3,7,9,5,2]

for i in range(0, len(list)-1):
    minval = list[i]
    minval_index = i
    for j in range(i+1, len(list)):
       if list[j] < minval:
            minval = list[j]
            minval_index = j
    list[minval_index] = list[i]
    list[i] = minval

print(list)

#+end_src

#+RESULTS:
: [2, 3, 5, 7, 8, 9]

