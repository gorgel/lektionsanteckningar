:PROPERTIES:
:ID:       4dc7bfa6-7ca0-45f2-bcb1-4e958b32d200
:END:
#+title: Vektorer och krafter 
#+LANGUAGE: sv
#+filetags:  
-----
- taggar ::
-----

* Kraftdefinition

En kraft är en vektorstorhet (med storlek och riktning) som verkar på föremål och kan förändra föremålets:

- Hastighet
- Form
- Rotation

* Olika typer av krafter

Kontaktkrafter: Uppstår när två föremål är i kontakt med varandra.

T.ex. Friktionskraft, Normalkraft och spännkraft.

Avståndskrafter: Kan uppstå mellan föremål utan att de är i kontakt med varandra. 

T.ex. Tyngdkraften, magnetisk kraft och elektrisk kraft.

** Tyngdkraft

Ritas från föremålets tyngdpunkt i riktning mot jordens centrum. 

#+DOWNLOADED: screenshot @ 2024-09-18 18:34:02
[[file:Olika_typer_av_krafter/2024-09-18_18-34-02_screenshot.png]]

** Normalkraft

Ritas från kontaktytan mellan de två föremålen. Alltid vinkelrätt mot kontaktytan. 

#+DOWNLOADED: screenshot @ 2024-09-18 18:34:16
[[file:Olika_typer_av_krafter/2024-09-18_18-34-16_screenshot.png]]

* Exempel

Rita ut krafterna som verkar på äpplet med korrekt storlek och riktning.

#+DOWNLOADED: screenshot @ 2024-09-18 18:24:16
[[file:Exempel/2024-09-18_18-24-16_screenshot.png]]

* Exempel

En vikt ligger still på ett bord när My börjar putta på den. 

Vilka krafter verkar på vikten?

Mer om friktionskraft kommer senare. 


#+DOWNLOADED: screenshot @ 2024-09-18 18:25:17
[[file:Exempel/2024-09-18_18-25-17_screenshot.png]]

* Kraftresultant vid två parallella krafter

Om krafterna är riktade åt samma håll så adderar man dem för att få resultanten. Resultanten är summan (eller resultatet) av två eller fler krafter

#+DOWNLOADED: screenshot @ 2022-09-19 20:41:07
[[file:Kraftresultatn_vid_två_parallella_krafter/2022-09-19_20-41-07_screenshot.png]]

Hur blir det om krafterna är riktade i motsatt riktning?

Vad fattas i figuren nedan?

#+DOWNLOADED: screenshot @ 2022-09-19 20:41:35
[[file:Kraftresultatn_vid_två_parallella_krafter/2022-09-19_20-41-35_screenshot.png]]

Här borde värdet på $F_1$ vara -2 eftersom den är riktat åt andra hållet. När vi adderar dessa två vektorer får vi alltså En resultant som är +2, vilket är riktning åt höger.

* Kraftresultant för två icke-parallella krafter

#+DOWNLOADED: screenshot @ 2022-09-19 20:44:48
[[file:Kraftresultant_för_två_icke-parallella_krafter/2022-09-19_20-44-48_screenshot.png]]

När vi har två ickeparallella vektorer så addedar vi dem genom att parallellförflytta den ena vektorn till den andra. Det spelar ingen roll vilken vi tar. VI kan tex flytta kraften $F_2$ och parallellförflytta så att dess bas ligger på $F_1$ huvud. Sedan kan vi dra en vektor från basen på $F_1$ till huvudet på $F_2$. Denna vektor kommer då vara resultanten. 

** Exempel

Rita ut kraftresultanten för de tre exemplen

#+DOWNLOADED: screenshot @ 2022-09-19 20:47:40
#+ATTR_HTML: :width 800px
[[file:Kraftresultant_för_två_icke-parallella_krafter/2022-09-19_20-47-40_screenshot.png]]


*** Lösning

#+DOWNLOADED: screenshot @ 2022-09-19 21:00:13
#+ATTR_HTML: :width 800px
[[file:Kraftresultant_för_två_icke-parallella_krafter/2022-09-19_21-00-13_screenshot.png]]

* Komposantuppdelning :noexport:

Kraften F nedan delas upp i en x-komposant och en y-komposant:

#+DOWNLOADED: screenshot @ 2022-09-19 20:51:40
[[file:Komposantuppdelning/2022-09-19_20-51-40_screenshot.png]]

** Exempel

Dela upp kraften F nedan i en x-komposant och en y-komposant:

#+DOWNLOADED: screenshot @ 2022-09-19 20:52:17
[[file:Komposantuppdelning/2022-09-19_20-52-17_screenshot.png]]

*** Lösning

#+DOWNLOADED: screenshot @ 2022-09-19 21:10:03
[[file:Komposantuppdelning/2022-09-19_21-10-03_screenshot.png]]

** Exempel

1 ruta motsvarar 1 N. Bestäm kraftresultantens storlek och riktning (vinkel)

#+DOWNLOADED: screenshot @ 2022-09-19 20:52:56
#+ATTR_HTML: :width 800px
[[file:Komposantuppdelning/2022-09-19_20-52-56_screenshot.png]]

*** Lösning

#+DOWNLOADED: screenshot @ 2022-09-19 21:18:06
[[file:Komposantuppdelning/2022-09-19_21-18-06_screenshot.png]]
