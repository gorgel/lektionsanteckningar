:PROPERTIES:
:ID:       2b857989-a894-4041-860f-647b12db17f2
:END:
#+title: Rörelsemängd 
#+LANGUAGE: sv
#+filetags:  
-----
- taggar ::
-----

* Definition rörelsemängd

Rörelsemängd är en vektorstorhet med storlek och riktning. 

Ett föremåls rörelsemängd är produkten av ett föremåls massa och hastighet:

$p = m \cdot v$

Enheten för rörelsemängd är $\frac{kg\cdot m}{s}$

#+DOWNLOADED: screenshot @ 2024-11-04 15:50:58
[[file:Definition_rörelsemängd/2024-11-04_15-50-58_screenshot.png]]


** exempel

En bil med massan 1200 kg färdas med hastigheten 72 km/h. Bestäm rörelsemängden.


*** Lösning


#+DOWNLOADED: screenshot @ 2022-11-08 18:35:58
[[file:Definition_rörelsemängd/2022-11-08_18-35-58_screenshot.png]]


* Samband mellan rörelsemängd och rörelseenergi

#+DOWNLOADED: screenshot @ 2022-11-08 18:36:30
[[file:Samband_mellan_rörelsemängd_och_rörelseenergi/2022-11-08_18-36-30_screenshot.png]]

** Exempel

Ett tåg har rörelsemängden $70 \cdot 10^6$  kgm/s. Tåget har en kinetisk energi på 7 GJ. Beräkna tågets massa.


*** Lösning

#+DOWNLOADED: screenshot @ 2022-11-08 18:38:06
[[file:Samband_mellan_rörelsemängd_och_rörelseenergi/2022-11-08_18-38-06_screenshot.png]]








