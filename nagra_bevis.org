:PROPERTIES:
:ID:       d64df677-e7f2-4142-b323-7990cc2d40ea
:END:
#+title:Några bevis
#+LANGUAGE: sv
#+filetags: 
-----
- taggar :: Binomialsatsen
-----

* Bevis av deriveringsregel

Vi ska visa att det för varje heltal $n>0$ gäller att $f(x) = x^n \Rightarrow f'(x)=nx^{n-1}$

För att göra detta ska vi använda oss av derivatans definition och binomialsatsen. Enligt derivatans definition har vi att

#+DOWNLOADED: screenshot @ 2021-12-27 16:46:00
[[file:Bevis_av_deriveringsregel/2021-12-27_16-46-00_screenshot.png]]


Om vi ska ta gränsvärdet på detta måste vi först fundera på vad vi kan göra med termen $(x+h)^n$. Vi kan använda oss av binomialsatsen för att utveckla termen

#+DOWNLOADED: screenshot @ 2021-12-27 16:47:29
[[file:Bevis_av_deriveringsregel/2021-12-27_16-47-29_screenshot.png]]


Med denna utvekcling kan vi skriva täljaren som

#+DOWNLOADED: screenshot @ 2021-12-27 16:48:22
[[file:Bevis_av_deriveringsregel/2021-12-27_16-48-22_screenshot.png]]

där vi kan eliminera första och sista termen med varandra. Om vi skriver ut hela uttrycket nu har vi istället

#+DOWNLOADED: screenshot @ 2021-12-27 16:49:47
[[file:Bevis_av_deriveringsregel/2021-12-27_16-49-47_screenshot.png]]

och här kan vi skriva om termerna framför $h$ som $A_1$, $A_2$ osv. Då får vi uttrycket 

#+DOWNLOADED: screenshot @ 2021-12-27 16:51:44
[[file:Bevis_av_deriveringsregel/2021-12-27_16-51-44_screenshot.png]]

som vi sedan kan förkorta bort ett h i varje term så vi får 

#+DOWNLOADED: screenshot @ 2021-12-27 16:52:18
[[file:Bevis_av_deriveringsregel/2021-12-27_16-52-18_screenshot.png]]
 
Detta betyder att vi kan ta gränsvärdet på detta uttryck istället

#+DOWNLOADED: screenshot @ 2021-12-27 16:53:21
[[file:Bevis_av_deriveringsregel/2021-12-27_16-53-21_screenshot.png]]

Och då ser vi att alla termet förutom $A_1$ går mot noll då h går mot noll. $A_1$-termen definierade vi ovan som

#+DOWNLOADED: screenshot @ 2021-12-27 16:55:07
[[file:Bevis_av_deriveringsregel/2021-12-27_16-55-07_screenshot.png]]

Vilket gör att

#+DOWNLOADED: screenshot @ 2021-12-27 16:55:32
[[file:Bevis_av_deriveringsregel/2021-12-27_16-55-32_screenshot.png]]

Vilket skulle bevisas.
